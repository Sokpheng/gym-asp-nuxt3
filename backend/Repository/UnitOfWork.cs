﻿using backend.IRepository;
using backend.Models;
using Microsoft.AspNetCore.Identity;

namespace backend.Repository
{
	public class UnitOfWork : IUnitOfWork
	{
		private readonly DatabaseContext _context;

		private IGenericRepository<ApplicationUser> _applicationUsers;
		private IGenericRepository<ApplicationRole> _applicationRoles;
		private IGenericRepository<ApplicationUserRole> _applicationUserRoles;
		private IGenericRepository<Appointment> _appointments;
		private IGenericRepository<Coach> _coaches;
		private IGenericRepository<Customer> _customers;
		private IGenericRepository<CustomerGroup> _customerGroups;
		private IGenericRepository<Group> _groups;
		private IGenericRepository<Schedule> _schedules;
		private IGenericRepository<Status> _statuses;
		private IGenericRepository<Specialization> _specializations;
		private IGenericRepository<Supervisor> _supervisors;

		public UnitOfWork(DatabaseContext context)
		{
			_context = context;
		}
		public IGenericRepository<ApplicationUser> ApplicationUsers => _applicationUsers ??= new GenericRepository<ApplicationUser>(_context);

		public IGenericRepository<ApplicationRole> ApplicationRoles => _applicationRoles ??= new GenericRepository<ApplicationRole>(_context);

		public IGenericRepository<ApplicationUserRole> ApplicationUserRoles => _applicationUserRoles ??= new GenericRepository<ApplicationUserRole>(_context);

		public IGenericRepository<Appointment> Appointments => _appointments ??= new GenericRepository<Appointment>(_context);

		public IGenericRepository<Coach> Coaches => _coaches ??= new GenericRepository<Coach>(_context);

		public IGenericRepository<Customer> Customers => _customers ??= new GenericRepository<Customer>(_context);

		public IGenericRepository<Group> Groups => _groups ??= new GenericRepository<Group>(_context);

		public IGenericRepository<Schedule> Schedules => _schedules ??= new GenericRepository<Schedule>(_context);

		public IGenericRepository<Status> Statuses => _statuses ??= new GenericRepository<Status>(_context);

		public IGenericRepository<Supervisor> Supervisors => _supervisors ??= new GenericRepository<Supervisor>(_context);

		public IGenericRepository<Specialization> Specializations => _specializations ??= new GenericRepository<Specialization>(_context);

		public IGenericRepository<CustomerGroup> CustomerGroups => _customerGroups ??= new GenericRepository<CustomerGroup>(_context);

		public void Dispose()
		{
			_context.Dispose();
			GC.SuppressFinalize(this);
		}

		public async Task Save()
		{
			await _context.SaveChangesAsync();
		}
	}
}
