interface IMenu {
  name: string;
  path: string;
  iconName?: string;
}
