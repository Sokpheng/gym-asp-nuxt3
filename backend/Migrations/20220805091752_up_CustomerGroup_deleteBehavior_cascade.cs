﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace backend.Migrations
{
	public partial class up_CustomerGroup_deleteBehavior_cascade : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropForeignKey(
				name: "FK_CustomerGroup_Customers_CustomerId" ,
				table: "CustomerGroup");

			migrationBuilder.DropForeignKey(
				name: "FK_CustomerGroup_Groups_GroupId" ,
				table: "CustomerGroup");

			migrationBuilder.AddForeignKey(
				name: "FK_CustomerGroup_Customers_CustomerId" ,
				table: "CustomerGroup" ,
				column: "CustomerId" ,
				principalTable: "Customers" ,
				principalColumn: "Id" ,
				onDelete: ReferentialAction.Cascade);

			migrationBuilder.AddForeignKey(
				name: "FK_CustomerGroup_Groups_GroupId" ,
				table: "CustomerGroup" ,
				column: "GroupId" ,
				principalTable: "Groups" ,
				principalColumn: "Id" ,
				onDelete: ReferentialAction.Cascade);
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropForeignKey(
				name: "FK_CustomerGroup_Customers_CustomerId" ,
				table: "CustomerGroup");

			migrationBuilder.DropForeignKey(
				name: "FK_CustomerGroup_Groups_GroupId" ,
				table: "CustomerGroup");

			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "43618df5-4c4f-447c-9b4c-777370ad8eb2");

			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "e14763ad-8395-48a5-ab1d-3d36fe086893");

			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "fd048a07-ede6-42cc-8282-15348a1b8bf1");

			migrationBuilder.InsertData(
				table: "Role" ,
				columns: new[] { "Id" , "ConcurrencyStamp" , "Name" , "NormalizedName" } ,
				values: new object[,]
				{
					{ "2773be01-6b15-4d90-a1c0-a9acda04a753", "68b03320-aa15-487b-afe3-2320e5945d88", "Customer", "CUSTOMER" },
					{ "c1b77c68-b96d-4191-a1d5-20feae060cad", "177d7c6b-ab46-4a1e-bc03-3fc06b46f436", "Coach", "COACH" },
					{ "ec53f61c-8abe-4717-9578-c1f895c24519", "14de54c6-4fab-4a9e-b4d4-048624c8a19e", "Supervisor", "SUPERVISOR" }
				});

			migrationBuilder.AddForeignKey(
				name: "FK_CustomerGroup_Customers_CustomerId" ,
				table: "CustomerGroup" ,
				column: "CustomerId" ,
				principalTable: "Customers" ,
				principalColumn: "Id" ,
				onDelete: ReferentialAction.SetNull);

			migrationBuilder.AddForeignKey(
				name: "FK_CustomerGroup_Groups_GroupId" ,
				table: "CustomerGroup" ,
				column: "GroupId" ,
				principalTable: "Groups" ,
				principalColumn: "Id" ,
				onDelete: ReferentialAction.SetNull);
		}
	}
}
