﻿namespace backend.Models
{
	public class Days
	{
		public Guid Id { get; set; }
		public string DayOfWeek { get; set; }

		public virtual ICollection<Schedule> Schedules { get; set; }
		public virtual ICollection<Appointment> Appointments { get; set; }

	}
}
