﻿using System.ComponentModel.DataAnnotations;

namespace backend.DTO
{
	public class GroupBaseDTO
	{
		public Guid Id { get; set; }
		[Required]
		[StringLength(50 , ErrorMessage = "Group name is limited to {2} to {1} characters." , MinimumLength = 2)]
		public string Name { get; set; }
		public string? Description { get; set; }
		[Required]
		public string CoachId { get; set; }
	}
	public class CreateGroupDTO : GroupBaseDTO
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		public DateTime CreatedAt { get; set; } = DateTime.UtcNow;

	}
	public class UpdateGroupDTO : GroupBaseDTO
	{
		public DateTime? UpdatedAt { get; set; } = DateTime.UtcNow;
	}
	public class GroupDTO : GroupBaseDTO
	{
		public virtual CoachDTO Coach { get; set; }
		public virtual ICollection<CustomerGroupDTO> CustomerGroups { get; set; }
		public virtual ICollection<ScheduleDTO> Schedules { get; set; }
	}
}
