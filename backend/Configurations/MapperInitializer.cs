﻿using AutoMapper;
using backend.DTO;
using backend.Models;

namespace backend.Configurations
{
	public class MapperInitializer : Profile
	{
		public MapperInitializer()
		{
			CreateMap<Appointment , AppointmentBaseDTO>().ReverseMap();
			CreateMap<Appointment , AppointmentDTO>().ReverseMap();
			CreateMap<Appointment , CreateAppointmentDTO>().ReverseMap();
			CreateMap<Appointment , UpdateAppointmentDTO>().ReverseMap();
			CreateMap<ApplicationUser , UserDTO>().ReverseMap();
			CreateMap<ApplicationUser , CreateUserDTO>().ReverseMap();
			CreateMap<ApplicationUser , UpdateUserDTO>().ReverseMap();
			CreateMap<ApplicationRole , RoleBaseDTO>().ReverseMap();
			CreateMap<ApplicationRole , RoleDTO>().ReverseMap();
			CreateMap<ApplicationRole , CreateRoleDTO>().ReverseMap();
			CreateMap<ApplicationRole , UpdateRoleDTO>().ReverseMap();
			CreateMap<ApplicationUserRole , AssignRoleDTO>().ReverseMap();
			CreateMap<ApplicationUserRole , UserRoleDTO>().ReverseMap();
			CreateMap<Coach , CoachBaseDTO>().ReverseMap();
			CreateMap<Coach , CoachDTO>().ReverseMap();
			CreateMap<Coach , CreateCoachDTO>().ReverseMap();
			CreateMap<Coach , UpdateCoachDTO>().ReverseMap();
			CreateMap<Customer , CustomerDTO>().ReverseMap();
			CreateMap<Customer , CreateCustomerDTO>().ReverseMap();
			CreateMap<Customer , UpdateCustomerDTO>().ReverseMap();
			CreateMap<Group , GroupBaseDTO>().ReverseMap();
			CreateMap<Group , GroupDTO>().ReverseMap();
			CreateMap<Group , CreateGroupDTO>().ReverseMap();
			CreateMap<Group , UpdateGroupDTO>().ReverseMap();
			CreateMap<CustomerGroup , CustomerGroupDTO>().ReverseMap();
			CreateMap<CustomerGroup , CustomerJoinGroupDTO>().ReverseMap();
			CreateMap<Schedule , ScheduleBaseDTO>().ReverseMap();
			CreateMap<Schedule , ScheduleDTO>().ReverseMap();
			CreateMap<Schedule , CreateScheduleDTO>().ReverseMap();
			CreateMap<Schedule , UpdateScheduleDTO>().ReverseMap();
			CreateMap<Status , StatusBaseDTO>().ReverseMap();
			CreateMap<Status , StatusDTO>().ReverseMap();
			CreateMap<Status , CreateStatusDTO>().ReverseMap();
			CreateMap<Status , UpdateStatusDTO>().ReverseMap();
			CreateMap<Specialization , SpecializationBaseDTO>().ReverseMap();
			CreateMap<Specialization , SpecializationDTO>().ReverseMap();
			CreateMap<Specialization , CreateSpecializationDTO>().ReverseMap();
			CreateMap<Specialization , UpdateSpecializationDTO>().ReverseMap();
			CreateMap<Supervisor , SupervisorBaseDTO>().ReverseMap();
			CreateMap<Supervisor , SupervisorDTO>().ReverseMap();
			CreateMap<Supervisor , CreateSupervisorDTO>().ReverseMap();
			CreateMap<Supervisor , UpdateSupervisorDTO>().ReverseMap();
		}
	}
}
