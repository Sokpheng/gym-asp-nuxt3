type CategoryItem = {
  title?: string;
  category: string;
  image: string;
  url: string;
};

export { CategoryItem };
