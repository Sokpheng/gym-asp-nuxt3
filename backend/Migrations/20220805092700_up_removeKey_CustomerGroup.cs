﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace backend.Migrations
{
	public partial class up_removeKey_CustomerGroup : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropPrimaryKey(
				name: "PK_CustomerGroup" ,
				table: "CustomerGroup");

			migrationBuilder.AddPrimaryKey(
				name: "PK_CustomerGroup" ,
				table: "CustomerGroup" ,
				column: "Id");

			migrationBuilder.CreateIndex(
				name: "IX_CustomerGroup_CustomerId" ,
				table: "CustomerGroup" ,
				column: "CustomerId");
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropPrimaryKey(
				name: "PK_CustomerGroup" ,
				table: "CustomerGroup");

			migrationBuilder.DropIndex(
				name: "IX_CustomerGroup_CustomerId" ,
				table: "CustomerGroup");

			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "2787f825-f086-4da1-b22d-d5c1a319d260");

			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "a68cd605-0778-4219-9c89-dc27a9a98245");

			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "e7b4817e-1219-4ca2-ac28-90a4bc08ce82");

			migrationBuilder.AddPrimaryKey(
				name: "PK_CustomerGroup" ,
				table: "CustomerGroup" ,
				columns: new[] { "CustomerId" , "GroupId" });

			migrationBuilder.InsertData(
				table: "Role" ,
				columns: new[] { "Id" , "ConcurrencyStamp" , "Name" , "NormalizedName" } ,
				values: new object[,]
				{
					{ "43618df5-4c4f-447c-9b4c-777370ad8eb2", "40639085-05f0-4214-b800-ce438f336092", "Supervisor", "SUPERVISOR" },
					{ "e14763ad-8395-48a5-ab1d-3d36fe086893", "b70704ea-5413-4e88-8d8a-f6acc12feffd", "Customer", "CUSTOMER" },
					{ "fd048a07-ede6-42cc-8282-15348a1b8bf1", "a5912bad-ffc8-446f-adc3-1e87a4a6c2b4", "Coach", "COACH" }
				});
		}
	}
}
