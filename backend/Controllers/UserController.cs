﻿using AutoMapper;
using backend.DTO;
using backend.IRepository;
using backend.Models;
using backend.Services;
using backend.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.IO;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace backend.Controllers
{
	//[Authorize]
	[Route("api/[controller]")]
	[ApiController]
	public class UserController : ControllerBase
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly ILogger<AccountController> _logger;
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _unitOfWork;
		private readonly IAuthManager _authManager;
		IWebHostEnvironment _environment;

		public UserController(UserManager<ApplicationUser> userManager , ILogger<AccountController> logger , IMapper mapper , IUnitOfWork unitOfWork , IAuthManager authManager , IWebHostEnvironment environment)
		{
			_userManager = userManager;
			_logger = logger;
			_mapper = mapper;
			_unitOfWork = unitOfWork;
			_authManager = authManager;
			_environment = environment;
		}

		// GET: api/<UserController>
		[HttpGet]
		public async Task<IActionResult> GetAllUsers([FromQuery] RequestParams requestParams)
		{
			var user = await _unitOfWork.ApplicationUsers.GetPagedList(requestParams , include: q => q.Include(x => x.UserRoles).ThenInclude(ur => ur.Role));
			var result = _mapper.Map<IList<UserDTO>>(user);
			return Ok(result);
		}

		[HttpGet]
		[Route("Customers")]
		public async Task<IActionResult> GetAllCustomers([FromQuery] RequestParams requestParams)
		{
			var customers = await _unitOfWork.Customers.GetPagedList(requestParams ,
				include: q => q.Include(x => x.User)
				.ThenInclude(ur => ur.UserRoles)
				.ThenInclude(r => r.Role));
			var result = _mapper.Map<IList<CustomerDTO>>(customers);
			return Ok(new ObjectResult("Customers retrived successfully.") { Value = result , StatusCode = Ok().StatusCode });
		}

		[HttpGet]
		[Route("Coaches")]
		public async Task<IActionResult> GetAllCoaches([FromQuery] RequestParams requestParams)
		{
			var coaches = await _unitOfWork.Coaches.GetPagedList(requestParams ,
				include: q => q.Include(x => x.User)
				.ThenInclude(ur => ur.UserRoles)
				.ThenInclude(r => r.Role)
				.Include(c => c.Status)
				.Include(s => s.Specialization));
			var result = _mapper.Map<IList<CoachDTO>>(coaches);
			return Ok(new ObjectResult("Coaches retrived successfully.") { Value = result , StatusCode = Ok().StatusCode });
		}

		[HttpGet]
		[Route("Supervisors")]
		public async Task<IActionResult> GetAllSupervisors([FromQuery] RequestParams requestParams)
		{
			var supervisors = await _unitOfWork.Supervisors.GetPagedList(requestParams ,
				include: q => q.Include(x => x.User)
				.ThenInclude(ur => ur.UserRoles)
				.ThenInclude(r => r.Role)
				.Include(c => c.Status));
			var result = _mapper.Map<IList<SupervisorDTO>>(supervisors);
			return Ok(new ObjectResult("Supervisors retrived successfully.") { Value = result , StatusCode = Ok().StatusCode });
		}

		// GET api/<UserController>/5
		[HttpGet("{id}")]
		public async Task<IActionResult> GetUsers(string id)
		{
			var user = await _unitOfWork.ApplicationUsers.Get(u => u.Id == id , include: q => q.Include(x => x.UserRoles).ThenInclude(ur => ur.Role));

			if (user == null)
				return NotFound(new ObjectResult("User has not found.") { StatusCode = NotFound().StatusCode });

			var result = _mapper.Map<UserDTO>(user);
			return Ok(new ObjectResult(result) { StatusCode = Ok().StatusCode });
		}

		[HttpGet]
		[Route("Customer/{id}")]
		public async Task<IActionResult> GetCustomer(string id)
		{
			var customer = await _unitOfWork.Customers.Get(u => u.Id == id ,
				include: q => q.Include(x => x.User)
				.ThenInclude(ur => ur.UserRoles)
				.ThenInclude(r => r.Role)
				//.Include(g => g.CustomerGroups)
				//.ThenInclude(g => g.Group)
				//.Include(a => a.Appointments)
				//.ThenInclude(c => c.Coach)
				);
			if (customer == null)
				return NotFound(new ObjectResult("Customer not found.") { StatusCode = NotFound().StatusCode });

			var result = _mapper.Map<CustomerDTO>(customer);

			return Ok(new ObjectResult(result) { StatusCode = Ok().StatusCode });
		}


		[HttpGet]
		[Route("Coach/{id}")]
		public async Task<IActionResult> GetCoach(string id)
		{
			var coach = await _unitOfWork.Coaches.Get(u => u.Id == id ,
				include: q => q.Include(x => x.User)
				.ThenInclude(ur => ur.UserRoles)
				.ThenInclude(r => r.Role)
				.Include(s => s.Specialization)
				//.Include(g => g.Groups).ThenInclude(g => g.CustomerGroups)
				//.Include(a => a.Appointments).ThenInclude(c => c.Customer)
				.Include(s => s.Status)
				);
			if (coach == null)
				return NotFound(new ObjectResult("Coach not found.") { StatusCode = NotFound().StatusCode });

			var result = _mapper.Map<CoachDTO>(coach);

			return Ok(new ObjectResult(result) { StatusCode = Ok().StatusCode });
		}

		[HttpGet]
		[Route("Supervisor/{id}")]
		public async Task<IActionResult> GetSupervisor(string id)
		{
			var supervisor = await _unitOfWork.Supervisors.Get(u => u.Id == id ,
				include: q => q.Include(x => x.User)
				.ThenInclude(ur => ur.UserRoles)
				.ThenInclude(r => r.Role)
				.Include(s => s.Status)
				);
			if (supervisor == null)
				return NotFound(new ObjectResult("Supervisor not found.") { StatusCode = NotFound().StatusCode });

			var result = _mapper.Map<SupervisorDTO>(supervisor);

			return Ok(new ObjectResult(result) { StatusCode = Ok().StatusCode });
		}

		// POST api/<UserController>
		[Authorize(Roles = "Supervisor")]
		[HttpPost]
		[Route("Coach")]
		public async Task<IActionResult> CreateCoach([FromBody] CreateCoachDTO coacheDTO)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState.Values);
			}

			var isEmailExist = await _unitOfWork.ApplicationUsers.Get(u => u.Email == coacheDTO.User.Email);
			if (isEmailExist != null)
				return Conflict(new ObjectResult("Email has already registered.") { StatusCode = Conflict().StatusCode });

			var user = _mapper.Map<Coach>(coacheDTO);
			user.User.UserName = coacheDTO.User.Email;
			user.User.CreatedAt = DateTime.UtcNow;
			//var result = await _userManager.CreateAsync(user , userDTO.Password);
			var result = await _userManager.CreateAsync(user.User , coacheDTO.User.Password);

			if (!result.Succeeded)
			{
				foreach (var error in result.Errors)
				{
					ModelState.AddModelError(error.Code , error.Description);
				}
				return BadRequest(ModelState);
			}

			await _userManager.AddToRoleAsync(user.User , Role.Coach.ToString());

			await _unitOfWork.Coaches.Insert(user);
			await _unitOfWork.Save();
			return Created($"~api/User/Coach/{user.Id}" , new ObjectResult("Coach Created Successfully.") { StatusCode = 201 });

		}

		// POST api/<UserController>
		//[Authorize(Roles = "Supervisor")]
		[HttpPost]
		[Route("Supervisor")]
		public async Task<IActionResult> CreateSupervisor([FromBody] CreateSupervisorDTO supervisorDTO)
		{
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState.Values);
			}

			var isEmailExist = await _unitOfWork.ApplicationUsers.Get(u => u.Email == supervisorDTO.User.Email);
			if (isEmailExist != null)
				return Conflict(new ObjectResult("Email has already registered.") { StatusCode = Conflict().StatusCode });

			var user = _mapper.Map<Supervisor>(supervisorDTO);
			user.User.UserName = supervisorDTO.User.Email;
			user.User.CreatedAt = DateTime.UtcNow;
			//var result = await _userManager.CreateAsync(user , userDTO.Password);
			var result = await _userManager.CreateAsync(user.User , supervisorDTO.User.Password);

			if (!result.Succeeded)
			{
				foreach (var error in result.Errors)
				{
					ModelState.AddModelError(error.Code , error.Description);
				}
				return BadRequest(ModelState);
			}

			await _userManager.AddToRoleAsync(user.User , Role.Supervisor.ToString());

			await _unitOfWork.Supervisors.Insert(user);
			await _unitOfWork.Save();
			return Created($"~api/User/Coach/{user.Id}" , new ObjectResult("Supervisor Created Successfully.") { StatusCode = 201 });
		}

		// PUT api/<UserController>/5
		[Authorize(Roles = "Customer")]
		[HttpPut]
		[Route("Customer/{id}")]
		public async Task<IActionResult> UpdateCustomer(string id , [FromBody] UpdateCustomerDTO customerDTO)
		{
			if (!ModelState.IsValid)
			{
				_logger.LogError($"Invalid UPDATE attempt in {nameof(UpdateCustomer)}");
				return BadRequest(ModelState.Values);
			}

			var user = await _userManager.FindByIdAsync(id);

			if (user == null)
				return NotFound(new ObjectResult("Customer has not found.") { StatusCode = NotFound().StatusCode });

			_mapper.Map(customerDTO.User , user);
			await _userManager.UpdateAsync(user);

			var customer = await _unitOfWork.Customers.Get(u => u.Id == user.Id ,
					include: x => x.Include(s => s.User).ThenInclude(ur => ur.UserRoles).ThenInclude(r => r.Role)
					);

			var result = _mapper.Map<CustomerDTO>(customer);
			return Ok(new ObjectResult("Customer Updated Successfully.") { StatusCode = Ok().StatusCode , Value = result });
		}

		[Authorize(Roles = "Coach")]
		[HttpPut]
		[Route("Coach/{id}")]
		public async Task<IActionResult> UpdateCoach(string id , [FromBody] UpdateCoachDTO coachDTO)
		{
			if (!ModelState.IsValid)
			{
				_logger.LogError($"Invalid UPDATE attempt in {nameof(UpdateCoach)}");
				return BadRequest(ModelState.Values);
			}

			var coach = await _unitOfWork.Coaches.Get(u => u.Id == id);
			if (coach == null)
				return NotFound(new ObjectResult("Coach has not found.") { StatusCode = NotFound().StatusCode });

			_mapper.Map(coachDTO , coach);

			var coachBase = _mapper.Map<CoachBaseDTO>(coach);

			coach.User = null;
			_mapper.Map(coachBase , coach);
			_unitOfWork.Coaches.Update(coach);

			await _unitOfWork.Save();

			var user = await _userManager.FindByIdAsync(id);
			_mapper.Map(coachDTO.User , user);
			await _userManager.UpdateAsync(user);
			//var result = _mapper.Map<UserDTO>(user);

			coach = await _unitOfWork.Coaches.Get(u => u.Id == user.Id ,
					include: x => x.Include(s => s.Status)
					.Include(s => s.Specialization)
					.Include(s => s.User).ThenInclude(ur => ur.UserRoles).ThenInclude(r => r.Role)
					);

			var result = _mapper.Map<CoachDTO>(coach);
			return Ok(new ObjectResult("Coach Update Successfully.") { StatusCode = Ok().StatusCode , Value = result });
		}

		[Authorize(Roles = "Supervisor")]
		[HttpPut]
		[Route("Supervisor/{id}")]
		public async Task<IActionResult> UpdateSupervisor(string id , [FromBody] UpdateSupervisorDTO supervisorDTO)
		{
			if (!ModelState.IsValid)
			{
				_logger.LogError($"Invalid UPDATE attempt in {nameof(UpdateSupervisor)}");
				return BadRequest(ModelState.Values);
			}

			var supervisor = await _unitOfWork.Supervisors.Get(u => u.Id == id);
			if (supervisor == null)
				return NotFound(new ObjectResult("Supervisor has not found.") { StatusCode = NotFound().StatusCode });

			_mapper.Map(supervisorDTO , supervisor);

			var supervisorBaseDTO = _mapper.Map<SupervisorBaseDTO>(supervisor);
			supervisor.User = null;
			_mapper.Map(supervisorBaseDTO , supervisor);
			_unitOfWork.Supervisors.Update(supervisor);

			await _unitOfWork.Save();

			var user = await _userManager.FindByIdAsync(id);
			_mapper.Map(supervisorDTO.User , user);
			await _userManager.UpdateAsync(user);
			//var result = _mapper.Map<UserDTO>(user);
			//supervisor.User = user;

			supervisor = await _unitOfWork.Supervisors.Get(u => u.Id == user.Id ,
					include: x => x.Include(s => s.Status)
					.Include(a => a.User).ThenInclude(ur => ur.UserRoles).ThenInclude(r => r.Role)
					);

			var result = _mapper.Map<SupervisorDTO>(supervisor);
			return Ok(new ObjectResult("Supervisor Updated Successfully.") { StatusCode = Ok().StatusCode , Value = result });
		}

		[Authorize]
		[HttpPut]
		[Route("Profile/{id}")]
		public async Task<IActionResult> UpdateProfile(string id , [FromForm] IFormFile profileImage)
		{
			_logger.LogInformation($"Update profile image attampt for user id: {id}");

			var user = await _unitOfWork.ApplicationUsers.Get(u => u.Id == id , include: x => x.Include(ur => ur.UserRoles).ThenInclude(r => r.Role));
			if (user == null)
				return NotFound(new ObjectResult("User has not found.") { StatusCode = NotFound().StatusCode });

			if (profileImage.Length > 0)
			{
				// Delete previous image
				if (user.ImageName != null)
					DeleteImage(user.ImageName);


				string fileName = Guid.NewGuid() + profileImage.FileName;
				UploadImage(profileImage , fileName);

				user.ImageName = fileName;
				user.ImageSrc = $"{Request.Scheme}://{Request.Host}{Request.PathBase}/images/{fileName}";
				_unitOfWork.ApplicationUsers.Update(user);
				await _unitOfWork.Save();

				var result = new RestImageDTO()
				{
					ImageSrc = user.ImageSrc ,
					ImageName = user.ImageName
				};

				//var result = _mapper.Map<UserDTO>(user);
				return Ok(new ObjectResult("Profile Update Successfully.") { StatusCode = Ok().StatusCode , Value = result });
			}
			else
			{
				_logger.LogError($"Update profile image attampt fail for user id: {id}");
				return Forbid();
			}
		}

		[Authorize]
		[HttpPost]
		[Route("ChangePassword")]
		public async Task<IActionResult> ChangePassword([FromBody] ChangePasswordDTO passwordDTO)
		{
			var user = await _userManager.FindByIdAsync(passwordDTO.UserId);
			if (user == null)
				return NotFound(new ObjectResult("User has not found.") { StatusCode = NotFound().StatusCode });
			var result = await _userManager.ChangePasswordAsync(user , passwordDTO.CurrentPassword , passwordDTO.NewPassword);

			if (result.Succeeded)
				return Ok(new ObjectResult("Password Changed Successfully.") { StatusCode = Ok().StatusCode });
			else
				return BadRequest(new ObjectResult("Password Changed Failed. Please try again.") { StatusCode = BadRequest().StatusCode });
		}


		// DELETE api/<UserController>/5
		[Authorize(Roles = "Supervisor")]
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(string id)
		{
			//_userManager.change
			var user = await _userManager.FindByIdAsync(id);
			if (user == null)
				return NotFound(new ObjectResult("User not found.") { StatusCode = NotFound().StatusCode });
			var result = await _userManager.DeleteAsync(user);

			if (result.Succeeded)
				return Ok(new ObjectResult("User deleted successfully.") { StatusCode = Ok().StatusCode });
			return BadRequest(new ObjectResult("Invalid user.") { Value = result , StatusCode = BadRequest().StatusCode });
		}

		[NonAction]
		public void UploadImage(IFormFile image , string fileName)
		{
			string uploadPath = _environment.ContentRootPath + "\\images\\";

			if (!Directory.Exists(uploadPath))
			{
				Directory.CreateDirectory(uploadPath);
			}

			using (FileStream fileStream = System.IO.File.Create(uploadPath + fileName))
			{
				image.CopyTo(fileStream);
				fileStream.Flush();
			}
		}

		[NonAction]
		public void DeleteImage(string fileName)
		{
			var imagePath = _environment.ContentRootPath + "\\images\\" + fileName;
			if (System.IO.File.Exists(imagePath))
				System.IO.File.Delete(imagePath);
		}
	}
}
