type PricesPlanItem = {
  type: string;
  price: number;
  feature: string;
  featureList: string[];
  image?: string;
};

export { PricesPlanItem };
