﻿using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models
{
	public class Group
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }

		[ForeignKey(nameof(Coach))]
		public string CoachId { get; set; }

		public DateTime? CreatedAt { get; set; }
		public DateTime? UpdatedAt { get; set; }

		public Coach Coach { get; set; }
		public virtual ICollection<CustomerGroup> CustomerGroups { get; set; }
		public virtual ICollection<Schedule> Schedules { get; set; }
	}
}
