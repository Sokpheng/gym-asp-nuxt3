import { relativeTimeRounding } from "moment";
import { useToast } from "tailvue";
import { ISelect } from "~~/interfaces/IInput";
import { IChangePassword } from "~~/interfaces/IUser";
import { user } from "~~/stores/user";

type userType = "COACH" | "SUPERVISOR" | "CUSTOMER";
type userProfile = {
  src: string;
  alt: string;
};

const getUrl = (type: userType) => {
  const url = computed<string>(() => {
    if (type == "CUSTOMER") return "/api/User";
    else if (type == "COACH") return "/api/User/Coach";
    else return "/api/User/Supervisor";
  });

  return { url };
};

const getUserProfile = (): userProfile => {
  const profileImage = computed<userProfile>(() => {
    const userStore = user();

    if (userStore.credentials?.user?.imageSrc) {
      return {
        src: userStore.credentials?.user?.imageSrc,
        alt: userStore.credentials?.user?.imageName,
      };
    } else {
      return {
        src: "/static/images/profile.svg",
        alt: "User Image",
      };
    }
  });
  return profileImage.value;
};

const updateUserProfile = async (e) => {
  console.log("e", e);
  const { $toast } = useNuxtApp();
  const userStore = user();

  const formData = new FormData();
  formData.append("profileImage", e.target.files[0], e.target.files[0].name);
  const resp = await apiPut(
    `/api/User/Profile/${userStore.credentials?.id}`,
    formData
  );

  if (resp?.statusCode === 200) {
    userStore.updateImage({
      imageName: resp?.value?.imageName,
      imageSrc: resp?.value?.imageSrc,
    });
    $toast.show({
      message: "Profile image updated successfully",
      type: "success",
    });
  } else {
    $toast.show({
      message: "Profile image update failed",
      type: "danger",
    });
  }
};

const createUser = async (userData: object | FormData, type: userType) => {
  const { $toast } = useNuxtApp();
  const { url } = getUrl(type);

  const resp = await apiPost(url.value, userData);
  if (resp?.statusCode == 201) {
    $toast.show({
      message: resp?.value,
      type: "success",
    });
    return resp?.value;
  } else {
    $toast.show({
      message: resp?.value,
      type: "danger",
    });
    return null;
  }
};

const updateUser = async (
  id: string,
  userData: object | FormData,
  type: userType
) => {
  const { $toast } = useNuxtApp();
  const { url } = getUrl(type);
  const resp = await apiPut(`${url.value}/${id}`, userData);
  if (resp?.statusCode == 200) {
    $toast.show({
      message: "Profile updated successfully",
      type: "success",
    });
    return resp?.value;
  } else {
    $toast.show({
      message: resp?.value,
      type: "danger",
    });
    return null;
  }
};

const updateCustomer = async (id: string, userData: object | FormData) => {
  const { $toast } = useNuxtApp();
  const resp = await apiPut(`/api/User/Customer/${id}`, userData);
  if (resp?.statusCode == 200) {
    $toast.show({
      message: "Profile updated successfully",
      type: "success",
    });
    return resp?.value;
  } else {
    $toast.show({
      message: resp?.value,
      type: "danger",
    });
    return null;
  }
};

const deleteUser = async (id: string) => {
  const resp = await apiDelete("/api/User", id);
  const { $toast } = useNuxtApp();

  if (resp?.statusCode == 200) {
    $toast.show(resp?.value);
    return true;
  } else {
    $toast.show("Error deleting user.");
    return false;
  }
};

const changePassword = async (password: IChangePassword) => {
  const userStore = user();
  const data = {
    userId: userStore.credentials?.id,
    ...password,
  };
  const resp = await apiPost(`/api/User/ChangePassword`, data);
  if (resp?.statusCode == 200) {
    useToast().success(resp?.value);
    return true;
  } else {
    useToast().danger(resp?.value || "Error updating password");
    return false;
  }
};

// Coach
const coachGetAll = async () => {
  const resp = await apiGet("/api/User/Coaches");
  if (resp?.statusCode == 200) {
    return resp?.value;
  } else {
    return [];
  }
};

const coachMapToSelete = (coaches: any[]) => {
  return coaches.map((coach) => {
    return {
      value: coach.id,
      label: `${coach?.user?.firstName} ${coach?.user?.lastName}`,
    };
  });
};

// Customer
const customerGetAll = async () => {
  const resp = await apiGet("/api/User/Customers");

  if (resp?.statusCode == 200) {
    return resp?.value;
  } else {
    return [];
  }
};

const customerGetById = async (id: string) => {
  const resp = await apiGet(`/api/User/Customer/${id}`);
  console.log("resp customer:", resp);
  if (resp?.statusCode == 200) {
    return resp?.value;
  } else {
    return null;
  }
};

const customerMapToSelect = (customers: any[]): ISelect[] => {
  return customers.map((customer) => {
    return {
      value: customer?.id,
      label: `${customer?.user?.firstName} ${customer?.user?.lastName}`,
    };
  });
};

export {
  createUser,
  updateUser,
  updateCustomer,
  deleteUser,
  getUserProfile,
  updateUserProfile,
  changePassword,
  coachGetAll,
  customerGetById,
  coachMapToSelete,
  customerGetAll,
  customerMapToSelect,
};
