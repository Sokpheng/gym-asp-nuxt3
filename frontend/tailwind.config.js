module.exports = {
  content: [
    `components/**/*.{vue,js}`,
    `layouts/**/*.vue`,
    `pages/**/*.vue`,
    `composables/**/*.{js,ts}`,
    `plugins/**/*.{js,ts}`,
    `App.{js,ts,vue}`,
    `app.{js,ts,vue}`,
    "node_modules/tailvue/dist/tailvue.es.js",
  ],
  theme: {
    container: {
      padding: {
        DEFAULT: "1rem",
        sm: "2rem",
        md: "3rem",
        lg: "4rem",
        xl: "5rem",
        "2xl": "6rem",
      },
      center: true,
    },
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1280px",
    },
    extends: {},
  },
  // variants for tailwindcss-dark-mode
  variants: {
    backgroundColor: [
      "dark",
      "dark-hover",
      "dark-group-hover",
      "dark-even",
      "dark-odd",
    ],
    borderColor: ["dark", "dark-disabled", "dark-focus", "dark-focus-within"],
    textColor: ["dark", "dark-hover", "dark-active", "dark-placeholder"],
  },
  // Plugins
  plugins: [
    require("@tailwindcss/typography"),
    require("@tailwindcss/line-clamp"),
    require("@tailwindcss/aspect-ratio"),
    require("daisyui"),
    require("tailwindcss-dark-mode")(),
  ],
  // enable to work with @nuxt/color-mode
  darkMode: "class",
  // variants for tailwindcss-dark-mode
  variants: {
    backgroundColor: [
      "dark",
      "dark-hover",
      "dark-group-hover",
      "dark-even",
      "dark-odd",
    ],
    borderColor: ["dark", "dark-disabled", "dark-focus", "dark-focus-within"],
    textColor: ["dark", "dark-hover", "dark-active", "dark-placeholder"],
  },
  daisyui: {
    styled: true,
    // bool | array
    themes: [
      "light",
      {
        light: {
          primary: "#F36100",
          secondary: "#6ad2dd",
          accent: "#fcc4db",
          neutral: "#382D39",
          "base-100": "#ffffff",
          info: "#219FF2",
          success: "#3CD7B3",
          warning: "#F9B343",
          error: "#ED5485",
        },
        dark: {
          ...require("daisyui/src/colors/themes")["[data-theme=dark]"],
          primary: "#F36100",
          secondary: "#f453a4",
          accent: "#b2f7f7",
          neutral: "#1F2037",
          "base-100": "#151515",
          info: "#4892CB",
          success: "#2ACF85",
          warning: "#D1AA10",
          error: "#FB6D51",
        },
      },
    ],
    base: true,
    utils: true,
    logs: true,
    rtl: false,
    prefix: "",
    darkTheme: "dark",
  },
};
