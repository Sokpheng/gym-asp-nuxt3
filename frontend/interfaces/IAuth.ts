interface IOptions {
  apiKey: string;
  Authorization?: string;
}

export { IOptions };
