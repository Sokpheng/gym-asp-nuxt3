﻿using AutoMapper;
using backend.DTO;
using backend.IRepository;
using backend.Models;
using backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace backend.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class RoleController : ControllerBase
	{
		private readonly ILogger<AccountController> _logger;
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _unitOfWork;
		private readonly IAuthManager _authManager;
		private readonly UserManager<ApplicationUser> _userManager;

		public RoleController(ILogger<AccountController> logger , IMapper mapper , IUnitOfWork unitOfWork , IAuthManager authManager , UserManager<ApplicationUser> userManager)
		{
			_logger = logger;
			_mapper = mapper;
			_unitOfWork = unitOfWork;
			_authManager = authManager;
			_userManager = userManager;
		}



		// GET: api/<StatusController>
		[AllowAnonymous]
		[HttpGet]
		public async Task<IActionResult> GetAll()
		{
			var roles = await _unitOfWork.ApplicationRoles.GetAll();
			var resutl = _mapper.Map<IList<RoleBaseDTO>>(roles);
			return Ok(resutl);
		}

		[HttpPost]
		[Route("AssignRole")]
		public async Task<IActionResult> AssignRole([FromBody] AssignRoleDTO roleDTO)
		{
			var user = await _unitOfWork.ApplicationUsers.Get(a => a.Id == roleDTO.UserId);
			var role = await _unitOfWork.ApplicationRoles.Get(r => r.Id == roleDTO.RoleId);

			await _userManager.AddToRoleAsync(user , role.Name);
			return Ok(new ObjectResult($"{user.UserName} has been assigned {role.Name} role") { StatusCode = Ok().StatusCode });
		}

		// GET api/<StatusController>/5
		[AllowAnonymous]
		[HttpGet("{id}")]
		public async Task<IActionResult> GetOne(string id)
		{
			var role = await _unitOfWork.ApplicationRoles.Get(r => r.Id == id);
			if (role == null)
				return NotFound(new ObjectResult("Role has not found.") { StatusCode = NotFound().StatusCode });
			var result = _mapper.Map<RoleBaseDTO>(role);
			return Ok(role);
		}
	}
}
