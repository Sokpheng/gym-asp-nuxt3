﻿namespace backend.DTO
{
	public class RestLoginDTO
	{
		public string Token { get; set; }
	}

	public class RestCustomerDTO : RestLoginDTO
	{
		public CustomerDTO User { get; set; }
	}

	public class RestCoachDTO : RestLoginDTO
	{
		public CoachDTO User { get; set; }
	}

	public class RestSupervisorDTO : RestLoginDTO
	{
		public SupervisorDTO User { get; set; }
	}
}
