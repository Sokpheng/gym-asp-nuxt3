﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace backend.Migrations
{
	public partial class update_customerGroup_id : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.AddColumn<Guid>(
				name: "Id" ,
				table: "CustomerGroup" ,
				type: "uuid" ,
				nullable: false ,
				defaultValue: new Guid("00000000-0000-0000-0000-000000000000"));
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "6ec2b0c4-55b0-42c7-9b29-d8bfb12e6d77");

			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "7146ea54-ad77-4c3d-8da3-52e4edfffb1d");

			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "eaf36993-c2df-4208-8a5b-29bf97fa44f0");

			migrationBuilder.DropColumn(
				name: "Id" ,
				table: "CustomerGroup");

			migrationBuilder.InsertData(
				table: "Role" ,
				columns: new[] { "Id" , "ConcurrencyStamp" , "Name" , "NormalizedName" } ,
				values: new object[,]
				{
					{ "008aa69e-509a-4054-9723-705d4b6efc9c", "ed15f8c1-8e4d-4f78-8cd5-96fc17ce0742", "Customer", "CUSTOMER" },
					{ "5a1e0ac8-f81a-4376-8e14-fdd9c2381ef2", "145a1807-b3e6-4bee-8e47-25b8bbc687e4", "Supervisor", "SUPERVISOR" },
					{ "fba039dd-69e6-4591-9b2d-b4301a8400e5", "4f45596d-b61d-47fd-97ce-19aa4bd55fbd", "Coach", "COACH" }
				});
		}
	}
}
