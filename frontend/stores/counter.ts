import { defineStore } from "pinia";
export const counter = defineStore("counter", () => {
  const count = ref<number | null>(0);

  const setCount = (value: number) => {
    count.value = value;
  };

  const increment = () => {
    count.value += 1;
  };

  return { count, setCount, increment };
});
