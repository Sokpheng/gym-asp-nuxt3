﻿using System.ComponentModel.DataAnnotations;

namespace backend.DTO
{
	public class RoleBaseDTO
	{
		public string Id { get; set; }
		[Required]
		[StringLength(50 , ErrorMessage = "Role name is limited to {2} to {1} characters." , MinimumLength = 2)]
		public string Name { get; set; }

		[Required]
		[StringLength(50 , ErrorMessage = "Role name is limited to {2} to {1} characters." , MinimumLength = 2)]
		public string NormalizedName { get; set; }
	}

	public class CreateRoleDTO
	{
		[Required]
		[StringLength(50 , ErrorMessage = "Role name is limited to {2} to {1} characters." , MinimumLength = 2)]
		public string Name { get; set; }

		[Required]
		[StringLength(50 , ErrorMessage = "Role name is limited to {2} to {1} characters." , MinimumLength = 2)]
		public string NormalizedName { get; set; }
	}

	public class UpdateRoleDTO : RoleBaseDTO
	{

	}

	public class RoleDTO : RoleBaseDTO
	{
		public virtual ICollection<UserRoleDTO> UserRoles { get; set; }
	}
}
