﻿namespace backend.DTO
{
	public class CoachSpecializationDTO
	{
		public string CoachId { get; set; }
		public Guid SpecializationId { get; set; }
		public virtual CoachDTO Coach { get; set; }
		public virtual SpecializationDTO Specialization { get; set; }
	}
}
