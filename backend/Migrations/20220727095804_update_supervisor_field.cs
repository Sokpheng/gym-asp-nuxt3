﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace backend.Migrations
{
    public partial class update_supervisor_field : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "08b65111-1fde-4407-ba39-af904ac0cbda");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "1bdd9e67-9ef4-40d2-9a76-73c4d55df873");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "a1c81934-a320-4f49-a707-4cbdaeef68db");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "08b65111-1fde-4407-ba39-af904ac0cbda", "6f98ba2b-b470-4598-a063-f59c075e8c33", "Coach", "COACH" },
                    { "1bdd9e67-9ef4-40d2-9a76-73c4d55df873", "d2cae66f-a3b4-474b-9fab-d47a04c70267", "Supervisor", "SUPERVISOR" },
                    { "a1c81934-a320-4f49-a707-4cbdaeef68db", "4d32d5a1-505c-42d4-9b6b-27ec241fbdd0", "Customer", "CUSTOMER" }
                });
        }
    }
}
