interface IStatus {
  id: string | number;
  name: string;
}

export { IStatus };
