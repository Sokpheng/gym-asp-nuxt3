﻿using AutoMapper;
using backend.DTO;
using backend.IRepository;
using backend.Models;
using backend.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace backend.Controllers
{
	[Authorize(Roles = "Supervisor")]
	[Route("api/[controller]")]
	[ApiController]
	public class StatusController : ControllerBase
	{
		private readonly ILogger<AccountController> _logger;
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _unitOfWork;
		private readonly IAuthManager _authManager;

		public StatusController(ILogger<AccountController> logger , IMapper mapper , IUnitOfWork unitOfWork , IAuthManager authManager)
		{
			_logger = logger;
			_mapper = mapper;
			_unitOfWork = unitOfWork;
			_authManager = authManager;
		}


		// GET: api/<StatusController>
		[AllowAnonymous]
		[HttpGet]
		public async Task<IActionResult> GetAll()
		{
			var status = await _unitOfWork.Statuses.GetAll();
			var result = _mapper.Map<IList<StatusBaseDTO>>(status);
			return Ok(new ObjectResult("Statuses retrived successfully.") { Value = result , StatusCode = Ok().StatusCode });
		}

		// GET api/<StatusController>/5
		[AllowAnonymous]
		[HttpGet("{id}")]
		public async Task<IActionResult> GetOne(Guid id)
		{
			var status = await _unitOfWork.Statuses.Get(s => s.Id == id , include: q => q.Include(x => x.Coaches).Include(x => x.Supervisors));
			if (status == null)
				return NotFound(new ObjectResult("Status has not found.") { StatusCode = NotFound().StatusCode });
			var result = _mapper.Map<StatusDTO>(status);
			return Ok(new ObjectResult("Statuses retrived successfully.") { Value = status , StatusCode = Ok().StatusCode });

		}

		// POST api/<StatusController>
		[HttpPost]
		public async Task<IActionResult> Create([FromBody] CreateStatusDTO statusDTO)
		{
			_logger.LogInformation("Create status attempt.");
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState.Values);
			}

			var isExist = await _unitOfWork.Statuses.Get(s => s.Name.ToLower() == statusDTO.Name.ToLower());
			if (isExist != null)
				return Conflict(new ObjectResult("Status has already exist.") { StatusCode = Conflict().StatusCode });

			var status = _mapper.Map<Status>(statusDTO);

			await _unitOfWork.Statuses.Insert(status);
			await _unitOfWork.Save();

			var result = _mapper.Map<StatusBaseDTO>(status);

			return Created("" , new ObjectResult("Status created successfully.") { StatusCode = 201 , Value = result });
		}

		// PUT api/<StatusController>/5
		[HttpPut("{id}")]
		public async Task<IActionResult> Update(Guid id , [FromBody] UpdateStatusDTO statusDTO)
		{
			if (!ModelState.IsValid)
			{
				_logger.LogError($"Invalid UPDATE attempt in {nameof(Update)}");
				return BadRequest(ModelState.Values);
			}

			var status = await _unitOfWork.Statuses.Get(s => s.Id == id);
			if (status == null)
				return NotFound(new ObjectResult("Status has not found.") { StatusCode = NotFound().StatusCode });

			var isDuplicateName = await _unitOfWork.Statuses.Get(s => s.Name.ToLower() == statusDTO.Name.ToLower());
			if (isDuplicateName != null)
				return Conflict(new ObjectResult("Status has already exist.") { StatusCode = Conflict().StatusCode });

			_mapper.Map(statusDTO , status);
			_unitOfWork.Statuses.Update(status);
			await _unitOfWork.Save();

			var result = _mapper.Map<CreateStatusDTO>(status);

			return Ok(new ObjectResult("Updated successfull.") { Value = result , StatusCode = Ok().StatusCode });
		}

		// DELETE api/<StatusController>/5
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(Guid id)
		{
			var status = await _unitOfWork.Statuses.Get(s => s.Id == id);
			if (status == null)
				return NotFound(new ObjectResult("Status has not found.") { StatusCode = NotFound().StatusCode });
			await _unitOfWork.Statuses.Delete(status.Id);
			await _unitOfWork.Save();
			return Ok(new ObjectResult("Status deleted successfully.") { StatusCode = Ok().StatusCode });
		}
	}
}
