﻿namespace backend.Models
{
	public enum Role
	{
		Supervisor,
		Coach,
		Customer
	}
}
