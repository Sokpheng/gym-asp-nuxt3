import { IRoutePath } from "~~/interfaces/IRoutePath";

const customerRoute: IRoutePath[] = [
  {
    url: "/customer/dashboard",
    name: "Dashboard",
    icon: "mdi:view-dashboard",
  },
  {
    url: "/customer/profile",
    name: "Profile",
    icon: "mdi:account",
  },
  {
    url: "/customer/appointments",
    name: "Appointments",
    icon: "mdi:clipboard-text-clock",
  },
  {
    url: "/customer/groups",
    name: "Groups",
    icon: "mdi:account-group",
  },
  {
    url: "/change-password",
    name: "Change Password",
    icon: "mdi:shield-key",
  },
];

const coachRoute: IRoutePath[] = [
  {
    url: "/coach/dashboard",
    name: "Dashboard",
    icon: "mdi:view-dashboard",
  },
  {
    url: "/coach/profile",
    name: "Profile",
    icon: "mdi:account",
  },
  {
    url: "/coach/appointments",
    name: "Appointments",
    icon: "mdi:clipboard-text-clock",
  },
  {
    url: "/coach/groups",
    name: "Groups",
    icon: "mdi:account-group",
  },
  {
    url: "/change-password",
    name: "Change Password",
    icon: "mdi:shield-key",
  },
];

const supervisorRoute: IRoutePath[] = [
  {
    url: "/supervisor/dashboard",
    name: "Dashboard",
    icon: "mdi:view-dashboard",
  },
  {
    url: "/supervisor/users",
    name: "User",
    icon: "mdi:account-multiple",
    children: [
      {
        url: "/supervisor/users/customers",
        name: "Customer",
      },
      {
        url: "/supervisor/users/coaches",
        name: "Coach",
      },
      {
        url: "/supervisor/users/supervisors",
        name: "Supervisor",
      },
    ],
  },
  {
    url: "/supervisor/appointments",
    name: "Appointments",
    icon: "mdi:clipboard-text-clock",
  },
  {
    url: "/supervisor/groups",
    name: "Groups",
    icon: "mdi:account-group",
  },
  {
    url: "/supervisor/role",
    name: "Role",
    icon: "clarity:assign-user-solid",
  },
  {
    url: "/supervisor/specialization",
    name: "Specialization",
    icon: "ic:baseline-fitness-center",
  },
  {
    url: "/supervisor/status",
    name: "Status",
    icon: "ic:sharp-checklist-rtl",
  },
  {
    url: "/change-password",
    name: "Change Password",
    icon: "mdi:shield-key",
  },
  {
    url: "/settings",
    name: "Settings",
    icon: "mdi:cog",
  },
];

export { customerRoute, coachRoute, supervisorRoute };
