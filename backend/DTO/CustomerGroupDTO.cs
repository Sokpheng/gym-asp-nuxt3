﻿namespace backend.DTO
{
	public class CustomerJoinGroupDTO
	{
		public string CustomerId { get; set; }
		public Guid GroupId { get; set; }
	}
	public class CustomerGroupDTO
	{
		public Guid Id { get; set; }
		public virtual CustomerDTO Customer { get; set; }
		public virtual GroupDTO Group { get; set; }
	}
}
