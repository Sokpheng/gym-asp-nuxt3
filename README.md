# gym-asp-nuxt3



## Getting started

### Backend (Using ASP.net Core 6, Postgresql 14 (pgAdmin 4))
- Install [pgAdmin 4](https://www.pgadmin.org/download/pgadmin-4-windows/)
- Open `pgAdmin 4` -> create new DB and restore this db `(gym)` from root dir
- Open project with IDE
- Go to `backend/models/DatabaseContext.cs`
- On `OnConfiguring()` fuction change the `db_string` with your own database connection strings
- Press `F5`
- In browser copy and past this url `https://localhost:7209/swagger/index.html`

### Frontend (Using Nuxt 3)
- Install [Node](https://nodejs.org/en/) lastest version
- This project using [yarn](https://yarnpkg.com/)
- Open `frontend` folder with VSCode or your favorite IDE
- Open `terminal` with the current project and run `yarn`
- Run `yarn dev`
- Open `http://localhost:3000/`

### Postman
- Open [Postman](https://www.postman.com/downloads/) and import collection `(Gym-api.postman_collection.json)` from root dir
- Check the varible `base_url` it should match the backend url `https://localhost:7209`

### User Roles (Supervisor, Coach, Customer)
- Supervisor (email, password): `admin@gmail.com`, `Admin@1234`
- Coach (email, password): `sokpheng@gmail.com`, `Password@123`
- Customer (email, password): `jamesbond@gmail.com`, `Password@123`

## License
Open to everyone.
