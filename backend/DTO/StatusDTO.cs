﻿using System.ComponentModel.DataAnnotations;

namespace backend.DTO
{
	public class StatusBaseDTO
	{
		public Guid? Id { get; set; }
		[Required]
		[StringLength(50 , ErrorMessage = "First name is limited to {2} to {1} characters." , MinimumLength = 2)]
		public string Name { get; set; }
	}
	public class UpdateStatusDTO : StatusBaseDTO
	{
	}

	public class CreateStatusDTO : StatusBaseDTO
	{
		public Guid Id { get; set; } = Guid.NewGuid();
	}

	public class StatusDTO : StatusBaseDTO
	{
		public virtual ICollection<CoachDTO> Coaches { get; set; }
		public virtual ICollection<SupervisorDTO> Supervisors { get; set; }
	}
}
