﻿namespace backend.DTO
{
	public class RestImageDTO
	{
		public string ImageName { get; set; }
		public string ImageSrc { get; set; }
	}
}
