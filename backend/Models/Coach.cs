﻿using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models
{
	public class Coach
	{
		[Key]
		[ForeignKey(nameof(User))]
		public string Id { get; set; }

		public string? WorkDayFrom { get; set; }
		public string? WorkDayTo { get; set; }

		public string? TimeFrom { get; set; }
		public string? TimeTo { get; set; }

		[ForeignKey(nameof(Status))]
		public Guid StatusId { get; set; }

		[ForeignKey(nameof(Specialization))]
		public Guid SpecializationId { get; set; }

		public Status Status { get; set; }
		public Specialization Specialization { get; set; }
		public virtual ApplicationUser User { get; set; }
		public virtual ICollection<Appointment> Appointments { get; set; }
		public virtual ICollection<Group> Groups { get; set; }
	}
}
