﻿using backend.Models;
using System.ComponentModel.DataAnnotations;

namespace backend.DTO
{
	public class UserBaseDTO
	{
		[Required]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }
		[Required]
		[StringLength(50 , ErrorMessage = "First name is limited to {2} to {1} characters." , MinimumLength = 2)]
		public string FirstName { get; set; }
		[Required]
		[StringLength(50 , ErrorMessage = "First name is limited to {2} to {1} characters." , MinimumLength = 2)]
		public string LastName { get; set; }

		public string? ImageSrc { get; set; }
		public string? ImageName { get; set; }

		[Required]
		public Gender Gender { get; set; }

		[Required]
		public DateTime DateOfBirth { get; set; }

		[DataType(DataType.PhoneNumber)]
		public string PhoneNumber { get; set; }
	}

	public class LoginUserDTO
	{
		[Required]
		[DataType(DataType.EmailAddress)]
		public string Email { get; set; }

		[Required]
		[StringLength(50 , ErrorMessage = "Your Password is limited to {2} to {1} characters" , MinimumLength = 4)]
		public string Password { get; set; }
	}

	public class UpdateUserDTO : UserBaseDTO
	{
		public DateTime? UpdatedAt { get; set; } = DateTime.UtcNow;
	}

	public class CreateUserDTO : UserBaseDTO
	{
		[Required]
		[StringLength(50 , ErrorMessage = "Your Password is limited to {2} to {1} characters" , MinimumLength = 4)]
		public string Password { get; set; }

		public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
	}

	public class UserDTO : UserBaseDTO
	{
		public string Id { get; set; }
		public virtual ICollection<UserRoleDTO> UserRoles { get; set; }
		//public virtual CustomerDTO Customer { get; set; }
		//public virtual CoachDTO Coach { get; set; }
		//public virtual Supervisor Supervisor { get; set; }
		public DateTime? UpdatedAt { get; set; }
		public DateTime CreatedAt { get; set; }
	}
}
