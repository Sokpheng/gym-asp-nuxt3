﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace backend.Migrations
{
	public partial class update_coach_time : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.AlterColumn<string>(
				name: "TimeTo" ,
				table: "Coaches" ,
				type: "text" ,
				nullable: true ,
				oldClrType: typeof(TimeOnly) ,
				oldType: "time without time zone" ,
				oldNullable: true);

			migrationBuilder.AlterColumn<string>(
				name: "TimeFrom" ,
				table: "Coaches" ,
				type: "text" ,
				nullable: true ,
				oldClrType: typeof(TimeOnly) ,
				oldType: "time without time zone" ,
				oldNullable: true);
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.AlterColumn<TimeOnly>(
				name: "TimeTo" ,
				table: "Coaches" ,
				type: "time without time zone" ,
				nullable: true ,
				oldClrType: typeof(string) ,
				oldType: "text" ,
				oldNullable: true);

			migrationBuilder.AlterColumn<TimeOnly>(
				name: "TimeFrom" ,
				table: "Coaches" ,
				type: "time without time zone" ,
				nullable: true ,
				oldClrType: typeof(string) ,
				oldType: "text" ,
				oldNullable: true);
		}
	}
}
