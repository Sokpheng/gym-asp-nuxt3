﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace backend.Migrations
{
    public partial class update_role : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Role",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "2a498672-4ee2-426e-baf5-9c380ecd064a", "9fd2fa37-9d50-467b-848e-49dd06630cae", "Customer", "CUSTOMER" },
                    { "bcbaba09-6f9a-4d10-9cd7-d05673a0ff2d", "75ee366e-a038-4390-b9cf-20763beffea1", "Supervisor", "SUPERVISOR" },
                    { "dee03d8c-942a-434c-bb65-6e61b5418a9d", "6190bf4b-dee3-429c-aeea-73a58d058db4", "Coach", "COACH" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "2a498672-4ee2-426e-baf5-9c380ecd064a");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "bcbaba09-6f9a-4d10-9cd7-d05673a0ff2d");

            migrationBuilder.DeleteData(
                table: "Role",
                keyColumn: "Id",
                keyValue: "dee03d8c-942a-434c-bb65-6e61b5418a9d");
        }
    }
}
