const useSecureCookie = (name: string, vlues?: any) => {
  const config = useRuntimeConfig();
  // console.log("config: ", config);

  const cookie = useCookie<string>(name, {
    secure: true,
    httpOnly: config.public.PRODUCTION,
    maxAge: 60 * 60 * 24 * 7,
    sameSite: "lax",
  });
  if (vlues) cookie.value = vlues;
  return cookie;
};

export { useSecureCookie };
