type MenuItem = {
  name: string;
  link: string;
};

export { MenuItem };
