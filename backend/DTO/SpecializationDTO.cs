﻿using System.ComponentModel.DataAnnotations;

namespace backend.DTO
{
	public class SpecializationBaseDTO
	{
		public Guid? Id { get; set; }
		[Required]
		[StringLength(50 , ErrorMessage = "Specialization is limited to {2} to {1} characters." , MinimumLength = 2)]
		public string Name { get; set; }
	}
	public class UpdateSpecializationDTO : SpecializationBaseDTO
	{
		public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;
	}

	public class CreateSpecializationDTO : SpecializationBaseDTO
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
	}
	public class SpecializationDTO : SpecializationBaseDTO
	{
		public virtual ICollection<CoachDTO> Coaches { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdatedAt { get; set; }
	}
}
