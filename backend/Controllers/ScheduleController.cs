﻿using AutoMapper;
using backend.DTO;
using backend.IRepository;
using backend.Models;
using backend.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace backend.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ScheduleController : ControllerBase
	{
		private readonly ILogger<AccountController> _logger;
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _unitOfWork;

		public ScheduleController(ILogger<AccountController> logger , IMapper mapper , IUnitOfWork unitOfWork)
		{
			_logger = logger;
			_mapper = mapper;
			_unitOfWork = unitOfWork;
		}

		// GET: api/<ScheduleController>
		[HttpGet]
		public async Task<IActionResult> GetAll([FromQuery] RequestParams requestParams)
		{
			var schedules = await _unitOfWork.Schedules.GetPagedList(requestParams , include: q => q.Include(x => x.Group));
			var result = _mapper.Map<IList<ScheduleDTO>>(schedules);
			return Ok(result);
		}

		// GET api/<ScheduleController>/5
		[HttpGet("{id}")]
		public async Task<IActionResult> Get(Guid id)
		{
			var schedule = await _unitOfWork.Schedules.Get(s => s.Id == id , include: q => q.Include(x => x.Group).ThenInclude(c => c.Coach));
			if (schedule == null)
				return NotFound(new ObjectResult("Schedule has not found.") { StatusCode = NotFound().StatusCode });
			var result = _mapper.Map<ScheduleDTO>(schedule);
			return Ok(result);
		}

		// GET api/<ScheduleController>/5
		[HttpGet]
		[Route("Coach/{id}")]
		public async Task<IActionResult> GetByCoach(string id , [FromQuery] RequestParams requestParams)
		{
			var schedules = await _unitOfWork.Schedules.GetPagedList(requestParams , s => s.Group.CoachId == id);
			var result = _mapper.Map<IList<ScheduleBaseDTO>>(schedules);
			return Ok(result);
		}

		// GET api/<ScheduleController>/5
		//[HttpGet]
		//[Route("Customer/{id}")]
		//public async Task<IActionResult> GetByCustomer(string id , [FromQuery] RequestParams requestParams)
		//{
		//	var schedules = await _unitOfWork.Schedules.GetPagedList(requestParams , s => s.Group.CustomerGroups.);
		//	var result = _mapper.Map<IList<ScheduleBaseDTO>>(schedules);
		//	return Ok(result);
		//}

		// POST api/<ScheduleController>
		[Authorize(Roles = "Coach, Supervisor")]
		[HttpPost]
		public async Task<IActionResult> Create([FromBody] CreateScheduleDTO scheduleDTO)
		{
			_logger.LogInformation("Create schedule attempt.");
			if (!ModelState.IsValid)
			{
				_logger.LogInformation("Invalid create schedule attempt.");
				return BadRequest(ModelState.Values);
			}

			var isScheduleExist = await _unitOfWork.Schedules.Get(s => s.GroupId == scheduleDTO.GroupId && s.StartDate == scheduleDTO.StartDate);
			if (isScheduleExist != null)
				return Conflict(new ObjectResult("Douplicated schedule. Please try again.") { StatusCode = Conflict().StatusCode });

			var schedule = _mapper.Map<Schedule>(scheduleDTO);

			await _unitOfWork.Schedules.Insert(schedule);
			await _unitOfWork.Save();

			return Created($"~api/Group/{schedule.Id}" , new ObjectResult(schedule) { StatusCode = 201 });
		}

		// PUT api/<ScheduleController>/5
		[Authorize(Roles = "Coach, Supervisor")]
		[HttpPut("{id}")]
		public async Task<IActionResult> Update(Guid id , [FromBody] UpdateScheduleDTO scheduleDTO)
		{
			_logger.LogInformation("Update schedule attempt.");
			if (!ModelState.IsValid)
			{
				_logger.LogInformation("Invalid update schedule attempt.");
				return BadRequest(ModelState.Values);
			}

			var schedule = await _unitOfWork.Schedules.Get(s => s.Id == id);
			if (schedule == null)
				return NotFound(new ObjectResult("Schedule does not exist.") { StatusCode = NotFound().StatusCode });

			if (schedule.StartDate == scheduleDTO.StartDate)
				return Conflict(new ObjectResult("Douplicated schedule. Please try again.") { StatusCode = Conflict().StatusCode });

			_mapper.Map(scheduleDTO , schedule);
			_unitOfWork.Schedules.Update(schedule);
			await _unitOfWork.Save();

			return Ok(schedule);
		}

		// DELETE api/<ScheduleController>/5
		[Authorize(Roles = "Coach, Supervisor")]
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(Guid id)
		{
			await _unitOfWork.Schedules.Delete(id);
			await _unitOfWork.Save();
			return Ok(new ObjectResult("Schedule deleted successfully.") { StatusCode = NoContent().StatusCode });
		}
	}
}
