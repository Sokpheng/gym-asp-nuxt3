﻿using AutoMapper;
using backend.DTO;
using backend.IRepository;
using backend.Models;
using backend.Services;
using backend.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace backend.Controllers
{
	//[Authorize(Roles = "Coach, Customer")]
	[Route("api/[controller]")]
	[ApiController]
	public class GroupController : ControllerBase
	{
		private readonly ILogger<AccountController> _logger;
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _unitOfWork;
		private readonly IAuthManager _authManager;
		private readonly UserManager<ApplicationUser> _userManager;

		public GroupController(ILogger<AccountController> logger , IMapper mapper , IUnitOfWork unitOfWork , IAuthManager authManager , UserManager<ApplicationUser> userManager)
		{
			_logger = logger;
			_mapper = mapper;
			_unitOfWork = unitOfWork;
			_authManager = authManager;
			_userManager = userManager;
		}

		// GET: api/<AppointmentController>
		[HttpGet]
		public async Task<IActionResult> GetAll([FromQuery] RequestParams requestParams)
		{
			var groups = await _unitOfWork.Groups.GetPagedList(requestParams , orderBy: x => x.OrderByDescending(s => s.CreatedAt));
			var result = _mapper.Map<IList<GroupBaseDTO>>(groups);
			return Ok(new ObjectResult(result) { StatusCode = Ok().StatusCode });
		}

		// GET api/<AppointmentController>/5
		[HttpGet("{id}")]
		public async Task<IActionResult> GetOne(Guid id)
		{
			var group = await _unitOfWork.Groups.Get(a => a.Id == id ,
				include: q => q.Include(x => x.CustomerGroups)
				.ThenInclude(c => c.Customer).ThenInclude(u => u.User)
				.Include(c => c.Coach).ThenInclude(s => s.Status)
				.Include(c => c.Coach).ThenInclude(u => u.User)
				.Include(c => c.Coach).ThenInclude(u => u.Specialization)
				.Include(s => s.Schedules)
				);
			if (group == null)
				return NotFound(new ObjectResult("Group has not found.") { StatusCode = NotFound().StatusCode });
			var result = _mapper.Map<GroupDTO>(group);
			return Ok(new ObjectResult(result) { StatusCode = Ok().StatusCode });
		}

		// Return list of group
		[HttpGet]
		[Route("Customer/{id}")]
		public async Task<IActionResult> GetByCustomer(string id , [FromQuery] RequestParams requestParams)
		{
			var group = await _unitOfWork.CustomerGroups.GetPagedList(requestParams , a => a.CustomerId == id , include: q => q.Include(x => x.Group).ThenInclude(c => c.Coach));
			if (group == null)
				return NotFound(new ObjectResult("Group has not found.") { StatusCode = NotFound().StatusCode });
			var result = _mapper.Map<IList<CustomerGroupDTO>>(group);
			return Ok(new ObjectResult(group) { StatusCode = Ok().StatusCode });
		}

		// Return list of group
		[HttpGet]
		[Route("Coach/{id}")]
		public async Task<IActionResult> GetByCoach(string id , [FromQuery] RequestParams requestParams)
		{
			var group = await _unitOfWork.Groups.GetPagedList(requestParams , a => a.CoachId == id , orderBy: x => x.OrderByDescending(s => s.CreatedAt));
			if (group == null)
				return NotFound(new ObjectResult("Group has not found.") { StatusCode = NotFound().StatusCode });
			var result = _mapper.Map<IList<GroupDTO>>(group);
			return Ok(new ObjectResult(group) { StatusCode = Ok().StatusCode });
		}

		// POST api/<AppointmentController>
		[Authorize(Roles = "Supervisor, Coach")]
		[HttpPost]
		public async Task<IActionResult> Create([FromBody] CreateGroupDTO groupDTO)
		{
			_logger.LogInformation("Create group attempt.");
			if (!ModelState.IsValid)
			{
				_logger.LogInformation("Invalid Create group attempt.");
				return BadRequest(ModelState.Values);
			}

			var isGroupExist = await _unitOfWork.Groups.Get(g => g.Name.ToLower() == groupDTO.Name.ToLower());
			if (isGroupExist != null)
				return Conflict(new ObjectResult("Group name already existed.") { StatusCode = Conflict().StatusCode });

			var group = _mapper.Map<Group>(groupDTO);
			await _unitOfWork.Groups.Insert(group);
			await _unitOfWork.Save();

			return Created($"~api/Group/{group.Id}" , new ObjectResult("Group created successfully.") { StatusCode = 201 });
		}

		// PUT api/<AppointmentController>/5
		[Authorize(Roles = "Supervisor, Coach")]
		[HttpPut("{id}")]
		public async Task<IActionResult> Update(Guid id , [FromBody] UpdateGroupDTO groupDTO)
		{
			_logger.LogInformation("Update appointment attempt.");
			if (!ModelState.IsValid)
			{
				_logger.LogInformation("Invalid update appointment attempt.");
				return BadRequest(ModelState.Values);
			}

			var group = await _unitOfWork.Groups.Get(a => a.Id == id);
			if (group == null)
				return NotFound(new ObjectResult("Group has not found.") { StatusCode = NotFound().StatusCode });

			_mapper.Map(groupDTO , group);
			_unitOfWork.Groups.Update(group);
			await _unitOfWork.Save();

			var result = _mapper.Map<UpdateGroupDTO>(group);

			return Ok(new ObjectResult(result) { StatusCode = Ok().StatusCode });
		}

		[Authorize(Roles = "Supervisor")]
		[HttpPost]
		[Route("JoinCustomer")]
		public async Task<IActionResult> JoinCustomer([FromBody] CustomerJoinGroupDTO groupDTO)
		{

			_logger.LogInformation("Join Customer attempt.");
			if (!ModelState.IsValid)
			{
				_logger.LogInformation("Invalid join customer attempt.");
				return BadRequest(ModelState.Values);
			}

			var isGroupExist = await _unitOfWork.Groups.Get(g => g.Id == groupDTO.GroupId);
			if (isGroupExist == null)
				return BadRequest(new ObjectResult("Group doesn't exist.") { StatusCode = NotFound().StatusCode });

			var isCustomerExist = await _unitOfWork.Customers.Get(c => c.Id == groupDTO.CustomerId , include: q => q.Include(x => x.User));
			if (isCustomerExist == null)
				return BadRequest(new ObjectResult("Customer doesn't exist.") { StatusCode = NotFound().StatusCode });

			var isCustomer = await _userManager.IsInRoleAsync(isCustomerExist.User , Role.Customer.ToString());
			if (isCustomer == false)
				return BadRequest(new ObjectResult("User role not allow.") { StatusCode = BadRequest().StatusCode });

			var isCustomerInGroup = await _unitOfWork.CustomerGroups.Get(g => g.GroupId == groupDTO.GroupId && g.CustomerId == groupDTO.CustomerId);
			if (isCustomerInGroup != null)
				return BadRequest(new ObjectResult("Customer has already in group.") { StatusCode = BadRequest().StatusCode });

			var customerGroup = _mapper.Map<CustomerGroup>(groupDTO);
			await _unitOfWork.CustomerGroups.Insert(customerGroup);
			await _unitOfWork.Save();
			return Ok(new ObjectResult("Join group successfully.") { StatusCode = Ok().StatusCode });
		}

		[Authorize(Roles = "Supervisor")]
		[HttpPost]
		[Route("RemoveCustomerGroup")]
		public async Task<IActionResult> RemoveCustomerGroup([FromBody] CustomerJoinGroupDTO groupDTO)
		{
			var customerGroup = await _unitOfWork.CustomerGroups.Get(cg => cg.CustomerId == groupDTO.CustomerId && cg.GroupId == groupDTO.GroupId);
			if (customerGroup == null)
				return BadRequest(new ObjectResult("Customer doesn't exist.") { StatusCode = NotFound().StatusCode });
			await _unitOfWork.CustomerGroups.Delete(customerGroup.Id);
			await _unitOfWork.Save();

			return Ok(new ObjectResult("Customer has been removed from group.") { StatusCode = Ok().StatusCode });
		}

		// DELETE api/<AppointmentController>/5
		[Authorize(Roles = "Supervisor, Coach")]
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(Guid id)
		{
			await _unitOfWork.Groups.Delete(id);
			await _unitOfWork.Save();
			return Ok(new ObjectResult("Group deleted successfully.") { StatusCode = Ok().StatusCode });
		}
	}
}
