﻿namespace backend.Models
{
	public class CustomerGroup
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		public string CustomerId { get; set; }
		public Guid GroupId { get; set; }

		public virtual Customer Customer { get; set; }
		public virtual Group Group { get; set; }
	}
}
