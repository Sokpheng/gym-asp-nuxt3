import { defineNuxtConfig } from "nuxt";

// https://v3.nuxtjs.org/api/configuration/nuxt.config
export default defineNuxtConfig({
  alias: {
    // fix error (__vite_ssr_import_0__.getter is not a function)
    yup: "yup/lib/index.d.ts",
  },

  autoImports: {
    dirs: [
      // Scan composables from nested directories
      "composables/**",
    ],
  },

  // ssr: false,
  // meta
  meta: {
    title: "Untitled",
    meta: [
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      {
        hid: "description",
        name: "description",
        content: "GYM",
      },
    ],
    link: [{ rel: "icon", type: "image/x-icon", href: "/favicon.ico" }],
  },

  static: {
    prefix: false,
  },

  // css
  css: ["~/assets/css/main.css"],

  // build modules
  buildModules: [
    "@pinia/nuxt",
    "@vueuse/nuxt",
    "@nuxtjs/tailwindcss",
    "@tailvue/nuxt",
    "@nuxtjs/color-mode",
  ],

  // Config colorMode
  colorMode: {
    preference: "system", // default value of $colorMode.preference
    fallback: "dark", // fallback value if not system preference found
    hid: "nuxt-color-mode-script",
    globalName: "__NUXT_COLOR_MODE__",
    componentName: "ColorScheme",
    classPrefix: "",
    classSuffix: "",
    storageKey: "nuxt-theme-color",
    dataValue: "theme", //activate data-theme in <html> tag (work with daisyUi)
  },

  // vueuse
  vueuse: {
    ssrHandlers: true,
  },

  // enanle auto import components
  components: true,

  // tailwind css
  tailwindcss: {
    cssPath: "~/assets/css/tailwind.css",
    configPath: "tailwind.config.js",
    exposeConfig: true,
    config: {},
    injectPosition: 0,
    viewer: true,
  },

  vite: {
    // @ts-ignore
    ssr: {
      noExternal: ["moment"],
    },
  },

  runtimeConfig: {
    // The private keys which are only available within server-side
    API_SECRET: process.env.API_KEY,
    BASE_URL: process.env.BASE_URL,
    // Keys within public, will be also exposed to the client-side
    public: {
      BASE_URL: process.env.BASE_URL,
      API_SECRET: process.env.API_KEY,
      PRODUCTION: process.env.NODE_ENV === "production" ? true : false,
    },
  },
});
