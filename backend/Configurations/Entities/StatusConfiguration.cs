﻿using backend.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace backend.Configurations.Entities
{
	public class StatusConfiguration : IEntityTypeConfiguration<Status>
	{
		public void Configure(EntityTypeBuilder<Status> builder)
		{
			builder.HasData(
			 new Status
			 {
				 Name = "Coach" ,
			 } ,
			 new Status
			 {
				 Name = "Head Coach" ,
			 } ,
			 new Status
			 {
				 Name = "Senior Coach" ,
			 } ,
			 new Status
			 {
				 Name = "Supervisor" ,
			 }
			);
		}
	}
}
