﻿using System.ComponentModel.DataAnnotations;

namespace backend.DTO
{
	public class CoachBaseDTO
	{
		public string? WorkDayFrom { get; set; }
		public string? WorkDayTo { get; set; }

		public string? TimeFrom { get; set; }
		public string? TimeTo { get; set; }
	}

	public class UpdateCoachDTO : CoachBaseDTO
	{
		public Guid StatusId { get; set; }
		public Guid SpecializationId { get; set; }
		public virtual UpdateUserDTO User { get; set; }
	}

	public class CreateCoachDTO : CoachBaseDTO
	{
		public Guid StatusId { get; set; }
		public Guid SpecializationId { get; set; }
		public virtual CreateUserDTO User { get; set; }
	}

	public class CoachDTO : CoachBaseDTO
	{
		public string Id { get; set; }
		public StatusBaseDTO Status { get; set; }
		public SpecializationBaseDTO Specialization { get; set; }
		public virtual UserDTO user { get; set; }
		//public virtual ICollection<CoachSpecializationDTO> CoachSpecializations { get; set; }
		//public virtual ICollection<AppointmentDTO> Appointments { get; set; }
		//public virtual ICollection<GroupDTO> Groups { get; set; }
	}
}
