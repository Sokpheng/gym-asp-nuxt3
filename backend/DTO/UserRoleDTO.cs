﻿using backend.Models;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.DTO
{
	public class AssignRoleDTO
	{
		public string RoleId { get; set; }
		public string UserId { get; set; }
	}

	public class UserRoleDTO
	{
		//public string UserId { get; set; }
		//public string RoleId { get; set; }
		public virtual UserDTO User { get; set; }
		public virtual CreateRoleDTO Role { get; set; }
	}
}
