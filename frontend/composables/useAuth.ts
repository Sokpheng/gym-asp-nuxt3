import {
  ICreateCoach,
  ICreateCustomer,
  ICreateSupervisor,
  ISignInUser,
} from "~~/interfaces/IUser";
import { user } from "~~/stores/user";

const registerCustomer = async (cridential: ICreateCustomer) => {
  const resp = await apiPost("/api/Account/Register", cridential);
  return resp;
};

const signInUser = async ({ email, password }: ISignInUser) => {
  const resp = await apiPost("/api/Account/Login", { email, password });
  if (resp?.value) {
    // store cookie
    const token = useSecureCookie("token");
    token.value = resp.value?.token;
    const store = user();
    store.setUser(resp.value?.user);
  }
  return resp;
};

const initUser = async () => {
  // const { userState, setUser } = userStore();
  // const api = useApi();
  // const token = useSecureCookie("token");
  // if (token.value && !userState) {
  //   const resp = await api.get("/api/get-user-login-by-token");
  //   console.log("response: ", resp);
  //   if (resp?.data) {
  //     token.value = resp.data?.token;
  //     setUser(resp.data);
  //   }
  // }
};

const logoutUser = () => {
  const store = user();
  const token = useCookie("token");
  token.value = null;
  // const userState = useCookie("user");
  // userState.value = null;
  store.removeUser();
  return navigateTo("/");
};

export { registerCustomer, signInUser, initUser, logoutUser };
