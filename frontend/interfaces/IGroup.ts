interface ICreateGroup {
  name: string;
  description?: string;
  coachId: string;
}

export { ICreateGroup };
