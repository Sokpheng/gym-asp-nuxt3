type FeatureItem = {
  title: string;
  description: string;
  icon: string;
};

export { FeatureItem };
