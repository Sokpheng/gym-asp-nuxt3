﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.DTO
{
	public class AppointmentBaseDTO
	{
		public Guid? Id { get; set; }
		[Required]
		[StringLength(50 , ErrorMessage = "Appointment title is limited to {2} to {1} characters." , MinimumLength = 2)]
		public string Title { get; set; }
		public bool IsActive { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
	}

	public class CreateAppointmentDTO : AppointmentBaseDTO
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		[Required]
		public string CoachId { get; set; }
		[Required]
		public string CustomerId { get; set; }
		public bool IsActive { get; set; } = true;
		public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
	}

	public class UpdateAppointmentDTO : AppointmentBaseDTO
	{
		[Required]
		public string CoachId { get; set; }
		[Required]
		public string CustomerId { get; set; }
		public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;
	}

	public class AppointmentDTO : AppointmentBaseDTO
	{
		public CoachDTO Coach { get; set; }
		public CustomerDTO Customer { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdatedAt { get; set; }
	}
}
