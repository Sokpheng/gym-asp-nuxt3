// import { user } from "~~/stores/user";

import { user } from "~~/stores/user";

const useUserRole = () => {
  // const userStore = localStorage.getItem("user");
  // const user = userStore ? JSON.parse(userStore) : null;

  const userStore = user();

  const role = computed<"customer" | "coach" | "supervisor" | null>(() => {
    if (!userStore) return null;

    if (userStore?.credentials?.user?.userRoles[0]?.role.name === "Customer") {
      return "customer";
    } else if (
      userStore?.credentials?.user?.userRoles[0]?.role.name === "Coach"
    ) {
      return "coach";
    } else if (
      userStore?.credentials?.user?.userRoles[0]?.role.name === "Supervisor"
    ) {
      return "supervisor";
    } else {
      return null;
    }
  });

  return { role };
};

export { useUserRole };
