﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models
{
	public class Supervisor
	{
		[Key]
		[ForeignKey(nameof(User))]
		public string Id { get; set; }

		[ForeignKey(nameof(Status))]
		public Guid? StatusId { get; set; }
		public Status Status { get; set; }

		public virtual ApplicationUser User { get; set; }
	}
}
