﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models
{
	public class Customer
	{
		[Key]
		[ForeignKey(nameof(User))]
		public string Id { get; set; }

		public virtual ApplicationUser User { get; set; }
		public virtual ICollection<Appointment> Appointments { get; set; }
		public virtual ICollection<CustomerGroup> CustomerGroups { get; set; }
	}
}
