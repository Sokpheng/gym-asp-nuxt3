﻿using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models
{
	public class Appointment
	{
		public Guid Id { get; set; }
		public string Title { get; set; }
		public bool IsActive { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }

		[ForeignKey(nameof(Coach))]
		public string CoachId { get; set; }

		[ForeignKey(nameof(Customer))]
		public string CustomerId { get; set; }

		public Coach Coach { get; set; }
		public Customer Customer { get; set; }

		public DateTime? CreatedAt { get; set; }
		public DateTime? UpdatedAt { get; set; }
	}
}
