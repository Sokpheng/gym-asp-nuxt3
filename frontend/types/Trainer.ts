type TrainerItem = {
  name: string;
  image: string;
  expertise?: string;
};

export { TrainerItem };
