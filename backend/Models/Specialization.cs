﻿namespace backend.Models
{
	public class Specialization
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		public string Name { get; set; }

		public DateTime? CreatedAt { get; set; } = DateTime.Now;
		public DateTime? UpdatedAt { get; set; }

		public virtual ICollection<Coach> Coaches { get; set; }
	}
}
