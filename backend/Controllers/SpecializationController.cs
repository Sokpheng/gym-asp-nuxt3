﻿using AutoMapper;
using backend.DTO;
using backend.IRepository;
using backend.Models;
using backend.Services;
using backend.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace backend.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class SpecializationController : ControllerBase
	{
		private readonly ILogger<AccountController> _logger;
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _unitOfWork;
		private readonly IAuthManager _authManager;

		public SpecializationController(ILogger<AccountController> logger , IMapper mapper , IUnitOfWork unitOfWork , IAuthManager authManager)
		{
			_logger = logger;
			_mapper = mapper;
			_unitOfWork = unitOfWork;
			_authManager = authManager;
		}

		// GET: api/<StatusController>
		[HttpGet]
		public async Task<IActionResult> GetAll([FromQuery] RequestParams requestParams)
		{
			var spec = await _unitOfWork.Specializations.GetPagedList(requestParams);
			var result = _mapper.Map<IList<CreateSpecializationDTO>>(spec);
			return Ok(new ObjectResult("Statuses retrived successfully.") { Value = result , StatusCode = Ok().StatusCode });
		}

		// GET api/<StatusController>/5
		[HttpGet("{id}")]
		public async Task<IActionResult> GetOne(Guid id)
		{
			var spec = await _unitOfWork.Specializations.Get(s => s.Id == id , include: q => q.Include(x => x.Coaches).ThenInclude(x => x.Status));
			if (spec == null)
				return NotFound(new ObjectResult("Specialization has not found.") { StatusCode = NotFound().StatusCode });
			var result = _mapper.Map<SpecializationDTO>(spec);
			return Ok(new ObjectResult("Statuses retrived successfully.") { Value = spec , StatusCode = Ok().StatusCode });
		}

		// POST api/<StatusController>
		[Authorize(Roles = "Supervisor")]
		[HttpPost]
		public async Task<IActionResult> Create([FromBody] CreateSpecializationDTO specializationDTO)
		{
			_logger.LogInformation("Create status attempt.");
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState.Values);
			}

			var isExist = await _unitOfWork.Specializations.Get(s => s.Name.ToLower() == specializationDTO.Name.ToLower());
			if (isExist != null)
				return Conflict(new ObjectResult("Specialization has already exist.") { StatusCode = Conflict().StatusCode });

			var spec = _mapper.Map<Specialization>(specializationDTO);

			await _unitOfWork.Specializations.Insert(spec);
			await _unitOfWork.Save();

			var result = _mapper.Map<CreateSpecializationDTO>(spec);

			return Created("" , new ObjectResult("Status created successfully.") { StatusCode = 201 , Value = result });
		}

		// PUT api/<StatusController>/5
		[Authorize(Roles = "Supervisor")]
		[HttpPut("{id}")]
		public async Task<IActionResult> Update(Guid id , [FromBody] UpdateSpecializationDTO specializationDTO)
		{
			if (!ModelState.IsValid)
			{
				_logger.LogError($"Invalid UPDATE attempt in {nameof(Update)}");
				return BadRequest(ModelState.Values);
			}

			var spec = await _unitOfWork.Specializations.Get(s => s.Id == id);
			if (spec == null)
				return NotFound(new ObjectResult("Specialization has not found.") { StatusCode = NotFound().StatusCode });

			var isDuplicateName = await _unitOfWork.Specializations.Get(s => s.Name.ToLower() == specializationDTO.Name.ToLower());
			if (isDuplicateName != null)
				return Conflict(new ObjectResult("Specialization has already exist.") { StatusCode = Conflict().StatusCode });

			_mapper.Map(specializationDTO , spec);
			_unitOfWork.Specializations.Update(spec);
			await _unitOfWork.Save();

			var result = _mapper.Map<CreateSpecializationDTO>(spec);

			return Ok(new ObjectResult("Updated successfull.") { Value = result , StatusCode = Ok().StatusCode });
		}

		// DELETE api/<StatusController>/5
		[Authorize(Roles = "Supervisor")]
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(Guid id)
		{
			var spec = await _unitOfWork.Specializations.Get(s => s.Id == id);
			if (spec == null)
				return NotFound(new ObjectResult("Specialization has not found.") { StatusCode = NotFound().StatusCode });
			await _unitOfWork.Specializations.Delete(spec.Id);
			await _unitOfWork.Save();
			return Ok(new ObjectResult("Specialization deleted successfully.") { StatusCode = Ok().StatusCode });
		}
	}
}
