import { ISelect } from "~~/interfaces/IInput";
import { ISpecialization } from "~~/interfaces/ISpecialization";
import { IStatus } from "~~/interfaces/IStatus";

const mapToSeleteSpecializations = (
  specializations: ISpecialization[]
): ISelect[] => {
  return specializations.map((s) => {
    return {
      value: s.id,
      label: s.name,
    };
  });
};

const mapToSeleteStatus = (statuses: IStatus[]): ISelect[] => {
  return statuses.map((s) => {
    return {
      value: s.id,
      label: s.name,
    };
  });
};

export { mapToSeleteSpecializations, mapToSeleteStatus };
