export default defineNuxtRouteMiddleware((to, from) => {
  const token = useSecureCookie("token");

  const { role } = useUserRole();

  if (!token.value || !role.value)
    return navigateTo("/login", { redirectCode: 301 });
});
