﻿using System.ComponentModel.DataAnnotations;

namespace backend.DTO
{
	enum DayOfWeek { Monday, Tuesday, Wendesday, Thurday, Friday, Saturday, Sunday }
	public class CreateDaysDTO
	{

		[Required, EnumDataType(typeof(DayOfWeek))]
		public string DayOfWeek { get; set; }
	}

	public class DaysDTO : CreateDaysDTO
	{
		public Guid Id { get; set; }

		public virtual ICollection<ScheduleDTO> Schedules { get; set; }
		public virtual ICollection<AppointmentDTO> Appointments { get; set; }
	}
}
