﻿using backend.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace backend.Configurations.Entities
{
	public class RoleConfiguration : IEntityTypeConfiguration<ApplicationRole>
	{
		public void Configure(EntityTypeBuilder<ApplicationRole> builder)
		{
			builder.HasData(
				new ApplicationRole
				{
					Name = "Supervisor" ,
					NormalizedName = "SUPERVISOR"
				} ,
				new ApplicationRole
				{
					Name = "Coach" ,
					NormalizedName = "COACH"
				} ,
				new ApplicationRole
				{
					Name = "Customer" ,
					NormalizedName = "CUSTOMER"
				}
			);
		}
	}
}
