export default defineNuxtRouteMiddleware(async (to, from) => {
  // await initUser();

  const token = useSecureCookie("token");
  // Check if the user has not login
  // if (userState == null && to.path !== "/login") {
  //   return navigateTo("/login", { redirectCode: 301 });
  // } else if (userState != null && to.path === "/login") {
  //   return navigateTo("/", { redirectCode: 301 });
  // }
  if (
    (token.value != null && to.path === "/login") ||
    (token.value != null && to.path === "/register")
  ) {
    return navigateTo("/", { redirectCode: 301 });
  }
});
