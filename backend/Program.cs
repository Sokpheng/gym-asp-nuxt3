using backend;
using backend.Configurations;
using backend.IRepository;
using backend.Models;
using backend.Repository;
using backend.Services;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.FileProviders;
using Microsoft.Identity.Web;

var builder = WebApplication.CreateBuilder(args);

ConfigurationManager Configuration = builder.Configuration;

//// Add services to the container.
//builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
//	.AddMicrosoftIdentityWebApi(builder.Configuration.GetSection("AzureAd"));

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

// Add Newtonsoft
builder.Services.AddControllers().AddNewtonsoftJson(op => op.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

// Add DbContext
builder.Services.AddDbContext<DatabaseContext>();

// Authetications
//builder.Services.AddAuthentication();
builder.Services.ConfigureIdentity(builder.Configuration);

// Add AutoMapper Service
//builder.Services.AddAutoMapper(typeof(MapperInitializer));
builder.Services.ConfigureAutoMapper();

builder.Services.AddTransient<IUnitOfWork , UnitOfWork>();
builder.Services.AddScoped<IAuthManager , AuthManager>();

// Add Jwt config
builder.Services.ConfigureJWT(Configuration);


//Config Cors
builder.Services.AddCors(p =>
{
	p.AddPolicy("AllowAllCors" , builder =>
		builder.AllowAnyOrigin()
		.AllowAnyMethod()
		.AllowAnyHeader()
	);
});


var app = builder.Build();
var env = builder.Environment;

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
	app.UseSwagger();
	app.UseSwaggerUI();
	app.UseHsts();
}

// Handle Error Exceptions
app.ConfigureExceptionHandler();

app.UseStaticFiles(new StaticFileOptions
{
	FileProvider = new PhysicalFileProvider(Path.Combine(env.ContentRootPath , "images")) ,
	RequestPath = "/images"
});

app.UseHttpsRedirection();
// add Cors middleware
app.UseCors("AllowAllCors");
app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
