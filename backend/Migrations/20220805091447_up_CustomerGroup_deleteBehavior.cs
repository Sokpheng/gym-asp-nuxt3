﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace backend.Migrations
{
	public partial class up_CustomerGroup_deleteBehavior : Migration
	{
		protected override void Up(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropForeignKey(
				name: "FK_CustomerGroup_Customers_CustomerId" ,
				table: "CustomerGroup");

			migrationBuilder.DropForeignKey(
				name: "FK_CustomerGroup_Groups_GroupId" ,
				table: "CustomerGroup");

			migrationBuilder.AddForeignKey(
				name: "FK_CustomerGroup_Customers_CustomerId" ,
				table: "CustomerGroup" ,
				column: "CustomerId" ,
				principalTable: "Customers" ,
				principalColumn: "Id" ,
				onDelete: ReferentialAction.SetNull);

			migrationBuilder.AddForeignKey(
				name: "FK_CustomerGroup_Groups_GroupId" ,
				table: "CustomerGroup" ,
				column: "GroupId" ,
				principalTable: "Groups" ,
				principalColumn: "Id" ,
				onDelete: ReferentialAction.SetNull);
		}

		protected override void Down(MigrationBuilder migrationBuilder)
		{
			migrationBuilder.DropForeignKey(
				name: "FK_CustomerGroup_Customers_CustomerId" ,
				table: "CustomerGroup");

			migrationBuilder.DropForeignKey(
				name: "FK_CustomerGroup_Groups_GroupId" ,
				table: "CustomerGroup");

			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "2773be01-6b15-4d90-a1c0-a9acda04a753");

			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "c1b77c68-b96d-4191-a1d5-20feae060cad");

			migrationBuilder.DeleteData(
				table: "Role" ,
				keyColumn: "Id" ,
				keyValue: "ec53f61c-8abe-4717-9578-c1f895c24519");

			migrationBuilder.InsertData(
				table: "Role" ,
				columns: new[] { "Id" , "ConcurrencyStamp" , "Name" , "NormalizedName" } ,
				values: new object[,]
				{
					{ "6ec2b0c4-55b0-42c7-9b29-d8bfb12e6d77", "344902d8-b80d-4723-bd7c-eb906934386b", "Supervisor", "SUPERVISOR" },
					{ "7146ea54-ad77-4c3d-8da3-52e4edfffb1d", "fb6766be-9ae5-44db-90ac-cfc160302b6b", "Customer", "CUSTOMER" },
					{ "eaf36993-c2df-4208-8a5b-29bf97fa44f0", "4a49a71a-3f26-4a13-ba39-555669b0717e", "Coach", "COACH" }
				});

			migrationBuilder.AddForeignKey(
				name: "FK_CustomerGroup_Customers_CustomerId" ,
				table: "CustomerGroup" ,
				column: "CustomerId" ,
				principalTable: "Customers" ,
				principalColumn: "Id" ,
				onDelete: ReferentialAction.Cascade);

			migrationBuilder.AddForeignKey(
				name: "FK_CustomerGroup_Groups_GroupId" ,
				table: "CustomerGroup" ,
				column: "GroupId" ,
				principalTable: "Groups" ,
				principalColumn: "Id" ,
				onDelete: ReferentialAction.Cascade);
		}
	}
}
