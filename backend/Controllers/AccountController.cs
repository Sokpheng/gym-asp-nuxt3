﻿using AutoMapper;
using backend.DTO;
using backend.IRepository;
using backend.Models;
using backend.Services;
using backend.Utils;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace backend.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AccountController : ControllerBase
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly ILogger<AccountController> _logger;
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _unitOfWork;
		private readonly IAuthManager _authManager;

		public AccountController(UserManager<ApplicationUser> userManager , ILogger<AccountController> logger , IMapper mapper , IUnitOfWork unitOfWork , IAuthManager authManager)
		{
			_userManager = userManager;
			_logger = logger;
			_mapper = mapper;
			_unitOfWork = unitOfWork;
			_authManager = authManager;
		}

		[HttpPost]
		[Route("Register")]
		public async Task<IActionResult> Register([FromBody] CreateCustomerDTO userDTO)
		{
			_logger.LogInformation($"Registration attemp for {userDTO.User.Email}");
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var user = _mapper.Map<Customer>(userDTO);
			user.User.UserName = userDTO.User.Email;
			user.User.CreatedAt = DateTime.UtcNow;
			//var result = await _userManager.CreateAsync(user , userDTO.Password);
			var result = await _userManager.CreateAsync(user.User , userDTO.User.Password);

			if (!result.Succeeded)
			{
				foreach (var error in result.Errors)
				{
					ModelState.AddModelError(error.Code , error.Description);
				}
				return BadRequest(new ObjectResult("Email has already registered.") { StatusCode = BadRequest().StatusCode });
			}

			await _userManager.AddToRoleAsync(user.User , Role.Customer.ToString());

			await _unitOfWork.Customers.Insert(user);
			await _unitOfWork.Save();
			return Created($"~api/User/{user.Id}" , new ObjectResult("Account Register Successfully.") { StatusCode = 201 });
		}

		[HttpPost]
		[Route("Login")]
		public async Task<IActionResult> Login([FromBody] LoginUserDTO userDTO)
		{
			_logger.LogInformation($"Registration attemp for {userDTO.Email}");
			if (!ModelState.IsValid)
			{
				return BadRequest(ModelState);
			}

			var user = await _authManager.ValidateUser(userDTO);

			if (user == null)
			{
				return Unauthorized(new ObjectResult("Invalid Email or Password.") { StatusCode = Unauthorized().StatusCode });
			}


			if (user.UserRoles.Any(x => x.Role.Name == Role.Customer.ToString()))
			{
				var customer = await _unitOfWork.Customers.Get(u => u.Id == user.Id ,
					include: x => x.Include(s => s.User).ThenInclude(ur => ur.UserRoles).ThenInclude(r => r.Role)
					);
				var result = _mapper.Map<CustomerDTO>(customer);
				string token = await _authManager.CreateToken();
				RestCustomerDTO resLogin = new RestCustomerDTO()
				{
					Token = token ,
					User = result ,
				};
				return Ok(new ObjectResult("Login success.") { StatusCode = Ok().StatusCode , Value = resLogin }); // RefreshToken error fix later
			}
			else if (user.UserRoles.Any(x => x.Role.Name == Role.Coach.ToString()))
			{
				var coach = await _unitOfWork.Coaches.Get(u => u.Id == user.Id ,
					include: x => x.Include(s => s.Status)
					.Include(s => s.Specialization)
					.Include(s => s.User).ThenInclude(ur => ur.UserRoles).ThenInclude(r => r.Role)
					);
				var result = _mapper.Map<CoachDTO>(coach);
				string token = await _authManager.CreateToken();
				RestCoachDTO resLogin = new RestCoachDTO()
				{
					Token = token ,
					User = result
				};
				return Ok(new ObjectResult("Login success.") { StatusCode = Ok().StatusCode , Value = resLogin }); // RefreshToken error fix later
			}
			else if (user.UserRoles.Any(x => x.Role.Name == Role.Supervisor.ToString()))
			{
				var supervisor = await _unitOfWork.Supervisors.Get(u => u.Id == user.Id ,
					include: x => x.Include(s => s.Status)
					.Include(a => a.User).ThenInclude(ur => ur.UserRoles).ThenInclude(r => r.Role)
					);
				var result = _mapper.Map<SupervisorDTO>(supervisor);
				string token = await _authManager.CreateToken();
				RestSupervisorDTO resLogin = new RestSupervisorDTO()
				{
					Token = token ,
					User = result
				};
				return Ok(new ObjectResult("Login success.") { StatusCode = Ok().StatusCode , Value = resLogin }); // RefreshToken error fix later
			}
			else
			{
				return Unauthorized(new ObjectResult("Invalid role or role has been removed.") { StatusCode = Unauthorized().StatusCode });
			}

			//var result = _mapper.Map<UserDTO>(user);
			//string token = await _authManager.CreateToken();
			//RestCustomerDTO resLogin = new RestCustomerDTO()
			//{
			//	Token = token ,
			//	User = result ,
			//};
			////result.UserRoles = ( ICollection<UserRoleDTO> )await _userManager.GetRolesAsync(user);
			////return Accepted(new TokenRequest { Token = await _authManager.CreateToken() , RefreshToken = await _authManager.CreateRefreshToken() });
			//return Ok(new ObjectResult("Login success.") { StatusCode = Ok().StatusCode , Value = resLogin }); // RefreshToken error fix later
		}

		//[HttpPost]
		//[Route("refreshtoken")]
		//public async Task<IActionResult> RefreshToken([FromBody] TokenRequest request)
		//{
		//	var tokenRequest = await _authManager.VerifyRefreshToken(request);
		//	if (tokenRequest is null)
		//	{
		//		return Unauthorized();
		//	}

		//	return Ok(tokenRequest);
		//}
	}
}
