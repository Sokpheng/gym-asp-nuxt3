import { defineStore } from "pinia";
import { ref } from "vue";
export const user = defineStore("user", () => {
  const credentials = ref<any | null>(null);

  const setUser = (value: any) => {
    credentials.value = value;
  };

  const updateImage = ({
    imageName,
    imageSrc,
  }: {
    imageName: string;
    imageSrc: string;
  }) => {
    credentials.value = {
      ...credentials.value,
      user: {
        ...credentials.value?.user,
        imageName,
        imageSrc,
      },
    };
  };

  const removeUser = () => {
    credentials.value = null;
  };

  return { credentials, setUser, removeUser, updateImage };
});
