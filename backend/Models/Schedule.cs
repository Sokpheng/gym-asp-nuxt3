﻿using System.ComponentModel.DataAnnotations.Schema;

namespace backend.Models
{
	public class Schedule
	{
		public Guid Id { get; set; }
		public string Title { get; set; }

		[ForeignKey(nameof(Group))]
		public Guid GroupId { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }

		public DateTime? CreatedAt { get; set; }
		public DateTime? UpdatedAt { get; set; }

		public Group Group { get; set; }
	}
}
