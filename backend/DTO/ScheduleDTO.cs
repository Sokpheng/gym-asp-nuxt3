﻿using System.ComponentModel.DataAnnotations;

namespace backend.DTO
{
	public class ScheduleBaseDTO
	{
		public Guid Id { get; set; }
		[Required]
		[StringLength(50 , ErrorMessage = "Schedule title is limited to {2} to {1} characters." , MinimumLength = 2)]
		public string Title { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
	}

	public class CreateScheduleDTO : ScheduleBaseDTO
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		public Guid GroupId { get; set; }
		public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
	}

	public class UpdateScheduleDTO : ScheduleBaseDTO
	{
		//public Guid GroupId { get; set; }
		public DateTime UpdatedAt { get; set; } = DateTime.UtcNow;
	}

	public class ScheduleDTO : ScheduleBaseDTO
	{
		public GroupBaseDTO Group { get; set; }
		public DateTime CreatedAt { get; set; }
		public DateTime UpdatedAt { get; set; }
	}
}
