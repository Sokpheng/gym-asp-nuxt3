﻿using System.ComponentModel.DataAnnotations;

namespace backend.DTO
{
	public class SupervisorBaseDTO
	{

	}
	public class CreateSupervisorDTO : SupervisorBaseDTO
	{
		public Guid? StatusId { get; set; }
		public virtual CreateUserDTO User { get; set; }
	}

	public class UpdateSupervisorDTO : SupervisorBaseDTO
	{
		public Guid? StatusId { get; set; }
		public virtual UpdateUserDTO User { get; set; }
	}

	public class SupervisorDTO : SupervisorBaseDTO
	{
		public string Id { get; set; }
		public virtual UserDTO User { get; set; }
		public StatusBaseDTO Status { get; set; }
	}
}
