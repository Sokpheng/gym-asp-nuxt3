﻿using AutoMapper;
using backend.DTO;
using backend.IRepository;
using backend.Models;
using backend.Services;
using backend.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace backend.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AppointmentController : ControllerBase
	{
		private readonly ILogger<AccountController> _logger;
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _unitOfWork;
		private readonly IAuthManager _authManager;

		public AppointmentController(ILogger<AccountController> logger , IMapper mapper , IUnitOfWork unitOfWork , IAuthManager authManager)
		{
			_logger = logger;
			_mapper = mapper;
			_unitOfWork = unitOfWork;
			_authManager = authManager;
		}

		// GET: api/<AppointmentController>
		[HttpGet]
		public async Task<IActionResult> GetAll([FromQuery] RequestParams requestParams)
		{
			var appointments = await _unitOfWork.Appointments.GetPagedList(requestParams , orderBy: x => x.OrderByDescending(o => o.CreatedAt));
			var result = _mapper.Map<IList<AppointmentBaseDTO>>(appointments);
			return Ok(new ObjectResult("Appointment retrived successfully.") { Value = result , StatusCode = Ok().StatusCode });
		}

		// GET api/<AppointmentController>/5
		[HttpGet("{id}")]
		public async Task<IActionResult> GetOne(Guid id)
		{
			var appointment = await _unitOfWork.Appointments.Get(a => a.Id == id ,
				include: q => q.Include(x => x.Customer).ThenInclude(c => c.User)
				.Include(c => c.Coach).ThenInclude(s => s.Status)
				.Include(c => c.Coach).ThenInclude(s => s.Specialization)
				.Include(c => c.Coach).ThenInclude(c => c.User)
				);
			if (appointment == null)
				return NotFound(new ObjectResult("Appointment has not found.") { StatusCode = NotFound().StatusCode });
			var result = _mapper.Map<AppointmentDTO>(appointment);
			return Ok(new ObjectResult("Appointment retrived successfully.") { Value = result , StatusCode = Ok().StatusCode });
		}

		[HttpGet]
		[Route("Customer/{id}")]
		public async Task<IActionResult> GetByCustomer(string id , [FromQuery] RequestParams requestParams)
		{
			var customer = await _unitOfWork.Customers.Get(c => c.Id == id);
			if (customer == null)
				return NotFound(new ObjectResult("Customer has not found.") { StatusCode = NotFound().StatusCode });

			var appointment = await _unitOfWork.Appointments.GetPagedList(requestParams , a => a.CustomerId == id ,
				include: q => q.Include(x => x.Coach).ThenInclude(c => c.User) , orderBy: x => x.OrderByDescending(o => o.CreatedAt));
			var result = _mapper.Map<IList<AppointmentDTO>>(appointment);

			return Ok(new ObjectResult("Appointment retrived successfully.") { Value = result , StatusCode = Ok().StatusCode });
		}

		[HttpGet]
		[Route("Coach/{id}")]
		public async Task<IActionResult> GetByCoach(string id , [FromQuery] RequestParams requestParams)
		{
			var customer = await _unitOfWork.Coaches.Get(c => c.Id == id);
			if (customer == null)
				return NotFound(new ObjectResult("Coach has not found.") { StatusCode = NotFound().StatusCode });

			var appointment = await _unitOfWork.Appointments.GetPagedList(requestParams , a => a.CoachId == id , include: q => q.Include(x => x.Customer));
			var result = _mapper.Map<IList<AppointmentDTO>>(appointment);

			return Ok(new ObjectResult("Appointment retrived successfully.") { Value = result , StatusCode = Ok().StatusCode });
		}

		// POST api/<AppointmentController>
		[Authorize(Roles = "Customer, Coach")]
		[HttpPost]
		public async Task<IActionResult> Create([FromBody] CreateAppointmentDTO appointmentDTO)
		{
			_logger.LogInformation("Create appointment attempt.");
			if (!ModelState.IsValid)
			{
				_logger.LogInformation("Invalid Create appointment attempt.");
				return BadRequest(ModelState.Values);
			}

			var appointment = _mapper.Map<Appointment>(appointmentDTO);
			await _unitOfWork.Appointments.Insert(appointment);
			await _unitOfWork.Save();

			return Created($"~api/Appointment/{appointment.Id}" , new ObjectResult("Appointment created successfully.") { StatusCode = 201 });
		}

		// PUT api/<AppointmentController>/5
		[Authorize(Roles = "Coach")]
		[HttpPut("{id}")]
		public async Task<IActionResult> Update(Guid id , [FromBody] UpdateAppointmentDTO appointmentDTO)
		{
			_logger.LogInformation("Update appointment attempt.");
			if (!ModelState.IsValid)
			{
				_logger.LogInformation("Invalid update appointment attempt.");
				return BadRequest(ModelState.Values);
			}

			var appointment = await _unitOfWork.Appointments.Get(a => a.Id == id);
			if (appointment == null)
				return NotFound(new ObjectResult("Appointment has not found.") { StatusCode = NotFound().StatusCode });

			_mapper.Map(appointmentDTO , appointment);
			_unitOfWork.Appointments.Update(appointment);
			await _unitOfWork.Save();

			var result = _mapper.Map<UpdateAppointmentDTO>(appointment);

			return Ok(new ObjectResult("Appointment updated successfully.") { Value = result , StatusCode = Ok().StatusCode });

		}

		// DELETE api/<AppointmentController>/5
		[Authorize(Roles = "Coach, Supervisor")]
		[HttpDelete("{id}")]
		public async Task<IActionResult> Delete(Guid id)
		{
			await _unitOfWork.Appointments.Delete(id);
			await _unitOfWork.Save();
			return Ok(new ObjectResult("Appointment deleted successfully.") { StatusCode = NoContent().StatusCode });
		}
	}
}
