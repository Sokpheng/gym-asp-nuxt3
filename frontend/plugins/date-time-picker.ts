import VueCtkDateTimePicker from "vue-ctk-date-time-picker";
import "vue-ctk-date-time-picker/dist/vue-ctk-date-time-picker.css";

export default defineNuxtPlugin((nuxtApp) => {
  nuxtApp.vueApp.component("DateTimePicker", VueCtkDateTimePicker);
});
