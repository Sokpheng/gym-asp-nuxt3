﻿using System.ComponentModel.DataAnnotations;

namespace backend.DTO
{
	public class CreateCustomerDTO
	{
		[Required]
		public virtual CreateUserDTO User { get; set; }
	}

	public class UpdateCustomerDTO
	{
		public virtual UpdateUserDTO User { get; set; }
	}

	public class CustomerDTO
	{
		public string Id { get; set; }
		public virtual UserDTO User { get; set; }
		//public virtual ICollection<AppointmentDTO> Appointments { get; set; }
		//public virtual ICollection<CustomerGroupDTO> CustomerGroups { get; set; }
	}
}
