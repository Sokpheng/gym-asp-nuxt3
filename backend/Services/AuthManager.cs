﻿using AutoMapper;
using backend.DTO;
using backend.IRepository;
using backend.Models;
using backend.Utils;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace backend.Services
{
	public class AuthManager : IAuthManager
	{
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly IConfiguration _configuration;
		private readonly IMapper _mapper;
		private readonly IUnitOfWork _unitOfWork;
		private ApplicationUser _user;

		//string issue = "GymAPI";
		//string refreshToken = "RefreshToken";

		public AuthManager(UserManager<ApplicationUser> userManager , IConfiguration configuration , IMapper mapper , IUnitOfWork unitOfWork)
		{
			_userManager = userManager;
			_configuration = configuration;
			_mapper = mapper;
			_unitOfWork = unitOfWork;
		}

		public async Task<string> CreateRefreshToken()
		{
			var jwtSettings = _configuration.GetSection("Jwt");
			var issuer = jwtSettings.GetSection("Issuer").Value;
			await _userManager.RemoveAuthenticationTokenAsync(_user , issuer , "RefreshToken");
			var newRefreshToken = await _userManager.GenerateUserTokenAsync(_user , issuer , "RefreshToken");
			var result = await _userManager.SetAuthenticationTokenAsync(_user , issuer , "RefreshToken" , newRefreshToken);
			return newRefreshToken;
		}

		public async Task<string> CreateToken()
		{
			var signingCredentials = GetSigningCredentials();
			var claims = await GetClaims();
			var token = GenerateTokenOptions(signingCredentials , claims);

			return new JwtSecurityTokenHandler().WriteToken(token);
		}

		//public async Task<bool> ValidateUser(LoginUserDTO userDTO)
		//{
		//	_user = await _userManager.FindByNameAsync(userDTO.Email);
		//	var validPassword = await _userManager.CheckPasswordAsync(_user , userDTO.Password);
		//	return (_user != null && validPassword);
		//}
		public async Task<ApplicationUser> ValidateUser(LoginUserDTO userDTO)
		{

			//_user = await _userManager.FindByEmailAsync(userDTO.Email);
			_user = await _unitOfWork.ApplicationUsers.Get(u => u.Email == userDTO.Email , include: q => q.Include(x => x.UserRoles).ThenInclude(r => r.Role));
			var validPassword = await _userManager.CheckPasswordAsync(_user , userDTO.Password);
			if (validPassword)
				return _user;
			return null;
		}

		public async Task<TokenRequest> VerifyRefreshToken(TokenRequest request)
		{
			var jwtSecurityTokenHandler = new JwtSecurityTokenHandler();
			var tokenContent = jwtSecurityTokenHandler.ReadJwtToken(request.Token);
			var jwtSettings = _configuration.GetSection("Jwt");
			var issuer = jwtSettings.GetSection("Issuer").Value;

			var username = tokenContent.Claims.ToList().FirstOrDefault(q => q.Type == ClaimTypes.Name)?.Value;
			_user = await _userManager.FindByNameAsync(username);
			try
			{
				var isValid = await _userManager.VerifyUserTokenAsync(_user , issuer , "RefreshToken" , request.RefreshToken);
				if (isValid)
				{
					return new TokenRequest { Token = await CreateToken() , RefreshToken = await CreateRefreshToken() };
				}
				await _userManager.UpdateSecurityStampAsync(_user);
			}
			catch (Exception ex)
			{
				throw ex;
			}

			return null;
		}

		private async Task<List<Claim>> GetClaims()
		{
			var claims = new List<Claim>
			 {
				 new Claim(ClaimTypes.Name, _user.UserName)
			 };

			var roles = await _userManager.GetRolesAsync(_user);

			foreach (var role in roles)
			{
				claims.Add(new Claim(ClaimTypes.Role , role));
			}

			return claims;
		}

		private JwtSecurityToken GenerateTokenOptions(SigningCredentials signingCredentials , List<Claim> claims)
		{
			var jwtSettings = _configuration.GetSection("Jwt");
			var expiration = DateTime.Now.AddDays(Convert.ToDouble(
				jwtSettings.GetSection("Lifetime").Value));

			var token = new JwtSecurityToken(
				issuer: jwtSettings.GetSection("Issuer").Value ,
				claims: claims ,
				expires: expiration ,
				signingCredentials: signingCredentials
				);

			return token;
		}

		private SigningCredentials GetSigningCredentials()
		{
			var jwtSettings = _configuration.GetSection("Jwt");
			var key = jwtSettings.GetSection("key").Value;
			var secret = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));

			return new SigningCredentials(secret , SecurityAlgorithms.HmacSha256);
		}
	}
}
