﻿using backend.DTO;
using backend.Models;
using backend.Utils;

namespace backend.Services
{
	public interface IAuthManager
	{
		Task<ApplicationUser> ValidateUser(LoginUserDTO userDTO);
		Task<string> CreateToken();
		Task<string> CreateRefreshToken();
		Task<TokenRequest> VerifyRefreshToken(TokenRequest request);
	}
}
