interface IChangePassword {
  currentPassword: string;
  newPassword: string;
}

interface IUser {
  email: string;
  firstName: string;
  lastName: string;
  gender: number;
  dateOfBirth: Date;
  phoneNumber: string;
  imageSrc?: string;
  imageName?: string;
}

interface ICreateUser extends IUser {
  password: string;
}

interface ICreateCustomer {
  user: ICreateUser;
}

interface IUpdateCustomer {
  user: IUser;
}

interface ICoach {
  statusId: string;
  specializationId: string;
  workDayFrom: string;
  workDayTo: string;
  timeFrom: string;
  timeTo: string;
}

interface ICreateCoach extends ICoach {
  user: ICreateUser;
}

interface IUpdateCoach extends ICoach {
  user: IUser;
}

interface ISupervisor {
  statusId: string;
}

interface ICreateSupervisor extends ISupervisor {
  user: ICreateUser;
}

interface IUpdateSupervisor extends ISupervisor {
  user: IUser;
}

interface ISignInUser extends IUser, ICreateUser {
  email: string;
  password: string;
}

export {
  IChangePassword,
  ISignInUser,
  ICreateCustomer,
  IUpdateCustomer,
  ICreateCoach,
  IUpdateCoach,
  ICreateSupervisor,
  ICreateUser,
  IUpdateSupervisor,
};
