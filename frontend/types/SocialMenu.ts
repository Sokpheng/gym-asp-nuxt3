type SocialLinkItem = {
  name: string;
  link: string;
  icon: string;
};

export { SocialLinkItem };
