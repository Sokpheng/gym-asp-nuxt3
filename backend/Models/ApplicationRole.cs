﻿using Microsoft.AspNetCore.Identity;

namespace backend.Models
{
	public class ApplicationRole : IdentityRole
	{
		public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
	}
}
