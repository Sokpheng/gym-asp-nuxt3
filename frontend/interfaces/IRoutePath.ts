interface IRoutePath {
  url: string;
  name: string;
  icon?: string;
  children?: IRoutePath[];
}

export { IRoutePath };
