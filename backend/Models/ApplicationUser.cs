﻿using Microsoft.AspNetCore.Identity;

namespace backend.Models
{
	public class ApplicationUser : IdentityUser
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string? ImageSrc { get; set; }
		public string? ImageName { get; set; }
		public Gender Gender { get; set; }
		public DateTime DateOfBirth { get; set; }

		public DateTime? CreatedAt { get; set; }
		public DateTime? UpdatedAt { get; set; }

		//public virtual ICollection<ApplicationUserToken> Tokens { get; set; }
		public virtual ICollection<ApplicationUserRole> UserRoles { get; set; }
		public virtual Customer Customer { get; set; }
		public virtual Coach Coach { get; set; }
		public virtual Supervisor Supervisor { get; set; }

	}
}
