﻿using backend.Models;
using Microsoft.AspNetCore.Identity;

namespace backend.IRepository
{
	public interface IUnitOfWork : IDisposable
	{
		IGenericRepository<ApplicationUser> ApplicationUsers { get; }
		IGenericRepository<ApplicationRole> ApplicationRoles { get; }
		IGenericRepository<ApplicationUserRole> ApplicationUserRoles { get; }
		IGenericRepository<Appointment> Appointments { get; }
		IGenericRepository<Coach> Coaches { get; }
		IGenericRepository<Customer> Customers { get; }
		IGenericRepository<Group> Groups { get; }
		IGenericRepository<CustomerGroup> CustomerGroups { get; }
		IGenericRepository<Schedule> Schedules { get; }
		IGenericRepository<Status> Statuses { get; }
		IGenericRepository<Specialization> Specializations { get; }
		IGenericRepository<Supervisor> Supervisors { get; }
		Task Save();
	}
}
