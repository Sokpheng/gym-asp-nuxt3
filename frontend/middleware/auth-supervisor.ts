export default defineNuxtRouteMiddleware((to, from) => {
  const token = useSecureCookie("token");

  const { role } = useUserRole();

  if (!token.value) return navigateTo("/login", { redirectCode: 301 });

  if (role.value !== "supervisor")
    return navigateTo("/", { redirectCode: 301 });
});
