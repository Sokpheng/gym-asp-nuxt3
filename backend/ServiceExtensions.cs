﻿using backend.Models;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using System.Reflection;
using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Microsoft.AspNetCore.Diagnostics;
using Serilog;
using backend.Utils;

namespace backend
{
	public static class ServiceExtensions
	{
		public static void ConfigureIdentity(this IServiceCollection services , IConfiguration configuration)
		{
			var jwtSettings = configuration.GetSection("Jwt");
			var issuer = jwtSettings.GetSection("Issuer").Value;

			var builder = services.AddIdentityCore<ApplicationUser>(q => q.User.RequireUniqueEmail = true);
			//var builder = services.AddIdentityCore<ApplicationUser>(options => options.SignIn.RequireConfirmedAccount = true);

			builder = new IdentityBuilder(builder.UserType , typeof(ApplicationRole) , services);
			//builder.AddTokenProvider(issuer , typeof(DataProtectorTokenProvider<ApplicationUser>));
			builder.AddTokenProvider<DataProtectorTokenProvider<ApplicationUser>>(TokenOptions.DefaultProvider);
			builder.AddEntityFrameworkStores<DatabaseContext>().AddDefaultTokenProviders();


			//services.AddIdentity<ApplicationUser , ApplicationRole>(options => options.Stores.MaxLengthForKeys = 128)
			//.AddEntityFrameworkStores<DatabaseContext>()
			//.AddDefaultTokenProviders();

		}

		public static void ConfigureAutoMapper(this IServiceCollection services)
		{
			services.AddAutoMapper(Assembly.GetExecutingAssembly());
		}

		public static void ConfigureJWT(this IServiceCollection services , IConfiguration Configuration)
		{
			var jwtSettings = Configuration.GetSection("Jwt");
			var issuer = jwtSettings.GetSection("Issuer").Value;
			var key = jwtSettings.GetSection("Key").Value;

			services.AddAuthentication(o =>
			{
				o.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
				o.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
				o.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
			})
			.AddJwtBearer(o =>
			{
				o.SaveToken = true;
				o.RequireHttpsMetadata = false;
				o.TokenValidationParameters = new TokenValidationParameters
				{
					ValidateIssuer = true ,
					ValidateAudience = false ,
					ValidateLifetime = true ,
					ValidateIssuerSigningKey = true ,
					//ValidIssuer = Configuration["Jwt:ValidIssuer"] ,
					ValidIssuer = issuer ,
					IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)) ,
				};
			});
		}

		public static void ConfigureExceptionHandler(this IApplicationBuilder app)
		{
			app.UseExceptionHandler(error =>
			{
				error.Run(async context =>
				{
					context.Response.StatusCode = StatusCodes.Status500InternalServerError;
					context.Response.ContentType = "application/json";
					var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
					if (contextFeature != null)
					{
						Log.Error($"Something Went Wrong in the {contextFeature.Error}");

						await context.Response.WriteAsync(new Error
						{
							StatusCode = context.Response.StatusCode ,
							Message = "Internal Server Error. Please Try Again Later."
						}.ToString());
					}
				});
			});
		}
	}
}
