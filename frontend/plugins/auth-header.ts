import { IOptions } from "~~/interfaces/IAuth";

export default defineNuxtPlugin((nuxtApp) => {
  const {
    public: { API_SECRET },
  } = useRuntimeConfig();
  const token = useSecureCookie("token");
  // console.log("token: ", token.value);

  let authHeader: IOptions;

  if (token.value) {
    authHeader = {
      Authorization: "Bearer " + token.value,
      apiKey: API_SECRET,
    };
  } else {
    authHeader = {
      apiKey: API_SECRET,
    };
  }
  nuxtApp.vueApp.provide("authHeader", authHeader);
  nuxtApp.provide("authHeader", authHeader);
});
