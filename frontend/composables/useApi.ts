const headers = {
  Accept: "*/*",
  // mode: "no-cors",
  // "Access-Control-Allow-Origin": "*",
};

// Check if the data is FormData or object
const getHeaders = (data: any) => {
  return data instanceof FormData
    ? headers
    : {
        ...headers,
        "Content-Type": "application/json",
      };
};

const apiGet = async (url: string, options?: HeadersInit) => {
  const { BASE_URL } = useRuntimeConfig();
  const { $authHeader } = useNuxtApp();
  return await fetch(BASE_URL + url, {
    headers: {
      ...$authHeader,
      ...headers,
    },
    method: "GET",
    ...options,
  })
    .then((res) => res.json())
    .then((res) => res)
    .catch((err) => console.log(err));
};

const apiPost = async (url: string, data: any, options?: HeadersInit) => {
  const { BASE_URL } = useRuntimeConfig();
  const { $authHeader } = useNuxtApp();

  return await fetch(BASE_URL + url, {
    headers: {
      ...$authHeader,
      ...getHeaders(data),
    },
    method: "POST",
    body: data instanceof FormData ? data : JSON.stringify(data),
    ...options,
  })
    .then((res) => res.json())
    .then((res) => res)
    .catch((err) => console.log(err));
};

const apiPut = async (url: string, data: any, options?: HeadersInit) => {
  const { BASE_URL } = useRuntimeConfig();
  const { $authHeader } = useNuxtApp();
  return await fetch(BASE_URL + url, {
    headers: {
      ...$authHeader,
      ...getHeaders(data),
    },
    method: "PUT",
    body: data instanceof FormData ? data : JSON.stringify(data),
    ...options,
  })
    .then((res) => res.json())
    .then((res) => res)
    .catch((err) => console.log(err));
};

const apiDelete = async (
  url: string,
  id: string | null,
  options?: HeadersInit
) => {
  const { BASE_URL } = useRuntimeConfig();
  const { $authHeader } = useNuxtApp();
  return await fetch(BASE_URL + url + "/" + id, {
    headers: {
      ...$authHeader,
      ...headers,
    },
    method: "DELETE",
    ...options,
  })
    .then((res) => res.json())
    .then((res) => res)
    .catch((err) => console.log(err));
};

export { apiGet, apiPost, apiPut, apiDelete };
