interface ISpecialization {
  id: string | number;
  name: string;
}

export { ISpecialization };
