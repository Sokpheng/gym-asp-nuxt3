﻿using Microsoft.EntityFrameworkCore;

namespace backend.Models
{
	public class Status
	{
		public Guid Id { get; set; } = Guid.NewGuid();
		public string Name { get; set; }

		public virtual ICollection<Coach> Coaches { get; set; }
		public virtual ICollection<Supervisor> Supervisors { get; set; }
	}
}
