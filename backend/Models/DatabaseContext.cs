﻿using backend.Configurations.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace backend.Models
{
	public class DatabaseContext : IdentityDbContext<ApplicationUser , ApplicationRole , string ,
		IdentityUserClaim<string> , ApplicationUserRole , IdentityUserLogin<string> ,
		IdentityRoleClaim<string> , IdentityUserToken<string>>
	{
		public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
		{

		}

		// Config with sqlite
		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=Gym_DB;User Id=hamburger;Password=root");
		}

		public DbSet<Appointment> Appointments { get; set; }
		public DbSet<Coach> Coaches { get; set; }
		public DbSet<Customer> Customers { get; set; }
		public DbSet<Group> Groups { get; set; }
		public DbSet<Specialization> Specializations { get; set; }
		public DbSet<Schedule> Schedules { get; set; }
		public DbSet<Status> Statuses { get; set; }
		public DbSet<Supervisor> Supervisors { get; set; }


		/// <summary>
		/// Table-per-type (TPT) => https://stackoverflow.com/questions/56132223/how-create-two-difference-table-with-identityuser
		/// </summary>
		/// <param name="builder"></param>
		protected override void OnModelCreating(ModelBuilder builder)
		{

			base.OnModelCreating(builder);

			builder.Entity<ApplicationUserRole>(userRole =>
			{
				userRole.HasKey(ur => new { ur.UserId , ur.RoleId });
				userRole.HasOne(ur => ur.Role).WithMany(r => r.UserRoles).HasForeignKey(ur => ur.RoleId).IsRequired();
				userRole.HasOne(ur => ur.User).WithMany(r => r.UserRoles).HasForeignKey(ur => ur.UserId).IsRequired();
				userRole.ToTable("UserRoles");
			});

			builder.Entity<ApplicationUser>(b =>
			{
				// Each User can have many UserTokens
				//b.HasMany(e => e.Tokens).WithOne(e => e.User).HasForeignKey(ut => ut.UserId).IsRequired();
				b.HasMany(e => e.UserRoles).WithOne(e => e.User).HasForeignKey(ur => ur.UserId).IsRequired();
				b.ToTable(name: "User");
			});

			builder.Entity<ApplicationRole>(b =>
			{
				// Each Role can have many entries in the UserRole join table
				b.HasMany(e => e.UserRoles).WithOne(e => e.Role).HasForeignKey(ur => ur.RoleId).IsRequired();
				b.ToTable(name: "Role");
			});

			builder.Entity<Customer>(c =>
			{
				c.HasOne(u => u.User).WithOne(u => u.Customer).HasForeignKey<Customer>(ur => ur.Id);
				c.HasMany(cg => cg.CustomerGroups).WithOne(c => c.Customer).HasForeignKey(cu => cu.CustomerId).IsRequired();
				c.HasMany(a => a.Appointments).WithOne(u => u.Customer).HasForeignKey(ur => ur.CustomerId);
			}
			);

			builder.Entity<Supervisor>(s =>
			{
				s.HasOne(a => a.User).WithOne(u => u.Supervisor).HasForeignKey<Supervisor>(ur => ur.Id);
			});

			builder.Entity<Group>(g =>
			{
				//g.HasMany(s => s.Schedules).WithOne(gs => gs.Group).HasForeignKey(g => g.Id).OnDelete(DeleteBehavior.Cascade);
				g.HasOne(co => co.Coach).WithMany(cog => cog.Groups).OnDelete(DeleteBehavior.SetNull);
				g.HasMany(cg => cg.CustomerGroups).WithOne(g => g.Group).HasForeignKey(gr => gr.GroupId).IsRequired();
			});

			builder.Entity<CustomerGroup>(cg =>
			{
				//cg.HasKey(uc => new { uc.CustomerId , uc.GroupId });
				cg.HasOne(c => c.Customer).WithMany(cg => cg.CustomerGroups).HasForeignKey(c => c.CustomerId).IsRequired().OnDelete(DeleteBehavior.Cascade);
				cg.HasOne(g => g.Group).WithMany(cg => cg.CustomerGroups).HasForeignKey(g => g.GroupId).IsRequired().OnDelete(DeleteBehavior.Cascade);
				cg.ToTable("CustomerGroup");
			});

			builder.Entity<Appointment>(a =>
			{
				a.HasOne(c => c.Coach).WithMany(ca => ca.Appointments).OnDelete(DeleteBehavior.Cascade);
				a.HasOne(c => c.Customer).WithMany(ca => ca.Appointments).OnDelete(DeleteBehavior.Cascade);
			});


			// Unique Field
			builder.Entity<Status>().HasIndex(s => s.Name).IsUnique();

			// Rename default table name

			builder.Entity<IdentityUserClaim<string>>(entity =>
			{
				entity.ToTable("UserClaims");
			});
			builder.Entity<IdentityUserLogin<string>>(entity =>
			{
				entity.ToTable("UserLogins");
			});
			builder.Entity<IdentityRoleClaim<string>>(entity =>
			{
				entity.ToTable("RoleClaims");
			});
			builder.Entity<IdentityUserToken<string>>(entity =>
			{
				entity.ToTable("UserTokens");
			});

			// Apply config on Add-Migration
			// Add Config user roles
			builder.ApplyConfiguration(new RoleConfiguration());
			//builder.ApplyConfiguration(new StatusConfiguration());
		}
	}
}
